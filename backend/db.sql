SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_ENGINE_SUBSTITUTION';

CREATE SCHEMA IF NOT EXISTS `simple_charging_service` DEFAULT CHARACTER SET utf8 ;
USE `simple_charging_service` ;


CREATE TABLE IF NOT EXISTS `simple_charging_service`.`role` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO role VALUES(1,'ADMIN');
INSERT INTO role VALUES(2,'CLIENT');
INSERT INTO role VALUES(3,'PROVIDER');

CREATE TABLE IF NOT EXISTS `simple_charging_service`.`user_account` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `login` VARCHAR(45) NOT NULL,
  `password` VARCHAR(100) NOT NULL,
  `first_name` VARCHAR(45) NOT NULL,
  `last_name` VARCHAR(45) NOT NULL,
  `role_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE INDEX `login_UNIQUE` (`login` ASC) VISIBLE,
  INDEX `fk_user_account_role1_idx` (`role_id` ASC) VISIBLE,
  CONSTRAINT `fk_user_account_role1`
    FOREIGN KEY (`role_id`)
    REFERENCES `simple_charging_service`.`role` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 23
DEFAULT CHARACTER SET = utf8;


CREATE TABLE IF NOT EXISTS `simple_charging_service`.`billing_account` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `balance` DECIMAL(5,2) NOT NULL,
  `user_account_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_billing_account_user_account1_idx` (`user_account_id` ASC) VISIBLE,
  CONSTRAINT `fk_billing_account_user_account1`
    FOREIGN KEY (`user_account_id`)
    REFERENCES `simple_charging_service`.`user_account` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 21
DEFAULT CHARACTER SET = utf8;


CREATE TABLE IF NOT EXISTS `simple_charging_service`.`service_category` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 71
DEFAULT CHARACTER SET = utf8;

INSERT INTO service_category VALUES(1,'Music');
INSERT INTO service_category VALUES(2,'Films');
INSERT INTO service_category VALUES(3,'Game');

CREATE TABLE IF NOT EXISTS `simple_charging_service`.`service` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) NOT NULL,
  `description` VARCHAR(1000) NOT NULL,
  `photo` LONGBLOB NULL DEFAULT NULL,
  `price` DECIMAL(10,2) NOT NULL,
  `service_category_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_service_service_category1_idx` (`service_category_id` ASC) VISIBLE,
  CONSTRAINT `fk_service_service_category1`
    FOREIGN KEY (`service_category_id`)
    REFERENCES `simple_charging_service`.`service_category` (`id`)
    ON DELETE RESTRICT
    ON UPDATE RESTRICT)
ENGINE = InnoDB
AUTO_INCREMENT = 74
DEFAULT CHARACTER SET = utf8;



CREATE TABLE IF NOT EXISTS `simple_charging_service`.`subscription_status` (
  `id` INT(11) NOT NULL,
  `name` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`id`))
ENGINE = InnoDB
DEFAULT CHARACTER SET = utf8;

INSERT INTO subscription_status VALUES(1,'Not enough money');
INSERT INTO subscription_status VALUES(2,'Active');
INSERT INTO subscription_status VALUES(3,'Finished');



CREATE TABLE IF NOT EXISTS `simple_charging_service`.`subscription` (
  `id` INT(11) NOT NULL AUTO_INCREMENT,
  `start_time` TIMESTAMP NOT NULL,
  `end_time` TIMESTAMP NOT NULL,
  `service_id` INT(11) NOT NULL,
  `billing_account_id` INT(11) NOT NULL,
  `subscription_status_id` INT(11) NOT NULL,
  PRIMARY KEY (`id`, `service_id`, `billing_account_id`),
  INDEX `fk_subscription_service1_idx` (`service_id` ASC) VISIBLE,
  INDEX `fk_subscription_billing_account1_idx` (`billing_account_id` ASC) VISIBLE,
  INDEX `fk_subscription_subscription_status1_idx` (`subscription_status_id` ASC) VISIBLE,
  CONSTRAINT `fk_subscription_billing_account1`
    FOREIGN KEY (`billing_account_id`)
    REFERENCES `simple_charging_service`.`billing_account` (`id`),
  CONSTRAINT `fk_subscription_service1`
    FOREIGN KEY (`service_id`)
    REFERENCES `simple_charging_service`.`service` (`id`),
  CONSTRAINT `fk_subscription_subscription_status1`
    FOREIGN KEY (`subscription_status_id`)
    REFERENCES `simple_charging_service`.`subscription_status` (`id`))
ENGINE = InnoDB
AUTO_INCREMENT = 3
DEFAULT CHARACTER SET = utf8;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
