package com.netcracker.backend;

public class BackendConstants {
    public static final int DEFAULT_BALANCE = 0;
    public static final int MLS_PER_DAY_IN_PROJECT = 5000;
    public static final long SEC_IN_ONE_DAY = 24 * 60 * 60;
    public static final long MLS_IN_ONE_SEC = 1000;
}
