package com.netcracker.backend.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;

@AllArgsConstructor
@Setter
@Getter
public class PageDto<T> {
    private List<T> content;
    private Long totalElements;
    private int totalPages;
    private int numberOfElements;
    private int currentPageNumber;
    private boolean last;
    private boolean first;

    public PageDto(Page<T> page) {
        this.content = page.getContent();
        this.totalElements = page.getTotalElements();
        this.totalPages = page.getTotalPages();
        this.numberOfElements = page.getNumberOfElements();
        this.first = page.isFirst();
        this.last = page.isLast();
        this.currentPageNumber = page.getNumber();
    }
}
