package com.netcracker.backend.service.impl;

import com.netcracker.backend.entity.ProviderCategory;
import com.netcracker.backend.repository.ProviderCategoryRepository;
import com.netcracker.backend.service.ProviderCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProviderCategoryServiceImpl implements ProviderCategoryService {
    @Autowired
    private ProviderCategoryRepository providerCategoryRepository;

    @Override
    public Iterable<ProviderCategory> findAll() {
        return providerCategoryRepository.findAll();
    }

    @Override
    public ProviderCategory create(ProviderCategory providerCategory) {
        return providerCategoryRepository.save(providerCategory);
    }

    @Override
    public Optional<ProviderCategory> findById(Long aLong) {
        return Optional.empty();
    }

    @Override
    public void deleteById(Long id) {
        providerCategoryRepository.deleteById(id);
    }

    @Override
    public ProviderCategory update(ProviderCategory providerCategory) {
        return providerCategoryRepository.save(providerCategory);
    }
}
