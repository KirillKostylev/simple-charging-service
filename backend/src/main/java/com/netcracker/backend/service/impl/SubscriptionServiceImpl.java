package com.netcracker.backend.service.impl;

import com.netcracker.backend.entity.BillingAccount;
import com.netcracker.backend.entity.Subscription;
import com.netcracker.backend.entity.SubscriptionStatus;
import com.netcracker.backend.repository.SubscriptionRepository;
import com.netcracker.backend.service.BillingAccountService;
import com.netcracker.backend.service.SubscriptionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.Optional;

import static com.netcracker.backend.entity.SubscriptionStatusEnum.ACTIVE;
import static com.netcracker.backend.entity.SubscriptionStatusEnum.NOT_ENOUGH_MONEY;

@Service
public class SubscriptionServiceImpl implements SubscriptionService {
    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionServiceImpl.class);

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Autowired
    private BillingAccountService billingAccountService;

    @Override
    public Iterable<Subscription> findAll() {
        return subscriptionRepository.findAll();
    }

    @Override
    public Optional<Subscription> findById(Long id) {
        return subscriptionRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        subscriptionRepository.deleteById(id);
    }

    @Override
    public Subscription create(Subscription subscription) {
        subscription.setStartTime(getCurrentTimestamp());
        double price = subscription.getProvider().getPrice();
        BillingAccount billingAccount = subscription.getBillingAccount();
        double balance = billingAccount.getBalance();
        if (balance >= price) {
            subscription.setStatus(ACTIVE.getStatus());
            billingAccount.setBalance(balance - price);
            billingAccountService.update(billingAccount);
        } else {
            subscription.setStatus(NOT_ENOUGH_MONEY.getStatus());
        }
        return subscriptionRepository.save(subscription);
    }

    @Override
    public Subscription update(Subscription subscription) {
        return subscriptionRepository.save(subscription);
    }

    @Override
    public void changeSubscriptionStatus(Subscription subscription, SubscriptionStatus status) {
        if (subscription == null) {
            LOGGER.error("Subscription cannot be null, status not changed.");
            throw new IllegalArgumentException();
        }

        if (status == null) {
            LOGGER.error("Status cannot be null, status not changed.");
            throw new IllegalArgumentException();
        }

        subscription.setStatus(status);
        subscriptionRepository.save(subscription);
        LOGGER.info("Subscription provider:" + subscription.getProvider().getName() + " Status was changed. Current status  = " + subscription.getStatus().getName());
    }

    @Override
    public Iterable<Subscription> findSubscriptionsFromUser(Long id) {
        return subscriptionRepository.findAllByBillingAccount_UserAccountIdOrderByStatusIdAsc(id);
    }

    private Timestamp getCurrentTimestamp() {
        return new Timestamp(System.currentTimeMillis());
    }
}
