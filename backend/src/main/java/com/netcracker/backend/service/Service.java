package com.netcracker.backend.service;

import java.util.Optional;

public interface Service<ENTITY, KEY> {
    ENTITY create(ENTITY entity);

    Optional<ENTITY> findById(KEY key);

    void deleteById(KEY key);

    ENTITY update(ENTITY entity);

    Iterable<ENTITY> findAll();
}
