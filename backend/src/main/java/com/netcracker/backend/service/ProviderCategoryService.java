package com.netcracker.backend.service;

import com.netcracker.backend.entity.ProviderCategory;

public interface ProviderCategoryService extends Service<ProviderCategory, Long> {
}
