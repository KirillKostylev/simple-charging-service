package com.netcracker.backend.service.exception;

public class EntryNotFoundException extends ServiceException {
    public EntryNotFoundException(String message) {
        super(message);
    }

    public EntryNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }
}
