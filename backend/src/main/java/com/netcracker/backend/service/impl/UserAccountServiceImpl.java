package com.netcracker.backend.service.impl;

import com.netcracker.backend.entity.Role;
import com.netcracker.backend.entity.UserAccount;
import com.netcracker.backend.repository.BillingAccountRepository;
import com.netcracker.backend.repository.SubscriptionRepository;
import com.netcracker.backend.repository.UserAccountRepository;
import com.netcracker.backend.service.UserAccountService;
import com.netcracker.backend.service.exception.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class UserAccountServiceImpl implements UserAccountService {
    @Autowired
    private UserAccountRepository userAccountRepository;

    @Autowired
    private SubscriptionRepository subscriptionRepository;

    @Override
    public UserAccount create(UserAccount userAccount) {
//        userAccount.setRole(Role.getClientRole());
        return userAccountRepository.save(userAccount);
    }

    @Override
    public List<UserAccount> findAll(String login) {
        List<UserAccount> resultList = userAccountRepository.findAll();
        if (login != null) {
            resultList = resultList.stream()
                    .filter(value -> value.getLogin().equals(login))
                    .collect(Collectors.toList());
        }
        return resultList;
    }

    @Override
    public UserAccount findByLogin(String login) throws EntryNotFoundException {
        Optional<UserAccount> user = userAccountRepository.findByLogin(login);
        if (user.isPresent()) {
            return user.get();
        }
        throw new EntryNotFoundException("User with login - " + login + " not found");
    }

    @Override
    public Integer countSubscribesByUserId(Long id) {
        Integer integer = subscriptionRepository.countSubscriptionByBillingAccount_UserAccountId(id);
        return integer;
    }


    @Override
    public void deleteById(Long id) {
        userAccountRepository.deleteById(id);
    }

    @Override
    public UserAccount update(UserAccount userAccount) {
        return userAccountRepository.save(userAccount);
    }

    @Override
    public Iterable<UserAccount> findAll() {
        return null;
    }

    @Override
    public Optional<UserAccount> findById(Long id) {
        return userAccountRepository.findById(id);
    }


}
