package com.netcracker.backend.service.impl;

import com.netcracker.backend.entity.BillingAccount;
import com.netcracker.backend.entity.Provider;
import com.netcracker.backend.entity.Subscription;
import com.netcracker.backend.service.BillingAccountService;
import com.netcracker.backend.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.sql.Timestamp;

import static com.netcracker.backend.BackendConstants.*;
import static com.netcracker.backend.entity.SubscriptionStatusEnum.*;

@Component
public class ScheduledTaskService {

    @Autowired
    private SubscriptionService subscriptionsService;

    @Autowired
    private BillingAccountService billingAccountService;

    @Scheduled(fixedDelay = MLS_PER_DAY_IN_PROJECT)
    public void takeOffMoney() {
        Iterable<BillingAccount> allBA = billingAccountService.findAll();
        for (BillingAccount ba : allBA) {
            for (Subscription subscription : ba.getSubscriptions()) {

                if (subscription.getStatus().getId().equals(FINISHED.getStatus().getId())) {
                    continue;
                }


                Provider provider = subscription.getProvider();
                if (ba.getBalance() >= provider.getPrice()) {
                    BillingAccount providerAccount = provider.getOwnerBillingAccount();
                    checkingSubscriptionOnEnoughMoney(subscription);
                    ba.setBalance(ba.getBalance() - provider.getPrice());  //taking money away
                    providerAccount.setBalance(providerAccount.getBalance() + provider.getPrice());
                } else {
                    changeSubscriptionStatusToNotEnoughMoney(subscription);
                }

                if (!NOT_ENOUGH_MONEY.getStatus().getId().equals(subscription.getStatus().getId())) {
                        addDayToSubscription(subscription);
                }

                subscriptionIsFinished(subscription);
            }
            billingAccountService.update(ba);
        }
    }

    private void changeSubscriptionStatusToNotEnoughMoney(Subscription subscription) {
        if (subscription.getStatus().getId().equals(ACTIVE.getStatus().getId())) {
            subscriptionsService.changeSubscriptionStatus(subscription, NOT_ENOUGH_MONEY.getStatus());
        }
    }

    private void checkingSubscriptionOnEnoughMoney(Subscription subscription) {
        if (subscription.getStatus().getId().equals(NOT_ENOUGH_MONEY.getStatus().getId())) {
            subscriptionsService.changeSubscriptionStatus(subscription, ACTIVE.getStatus());
        }
    }

    private void addDayToSubscription(Subscription subscription) {
        Timestamp startTime = subscription.getStartTime();
        long time = startTime.getTime();
        long timeInSec = time / MLS_IN_ONE_SEC;
        long newTimeInSec = timeInSec + SEC_IN_ONE_DAY;
        Timestamp newStartTime = new Timestamp(newTimeInSec * MLS_IN_ONE_SEC);
        subscription.setStartTime(newStartTime);
    }

    private void subscriptionIsFinished(Subscription subscription) {
        if (subscription.getStartTime().after(subscription.getEndTime())) {
            subscriptionsService.changeSubscriptionStatus(subscription, FINISHED.getStatus());
        }
    }
}


