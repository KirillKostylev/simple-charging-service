package com.netcracker.backend.service;

import com.netcracker.backend.dto.PageDto;
import com.netcracker.backend.entity.Provider;

public interface ProviderService extends Service<Provider, Long> {
    Iterable<Provider> findAllByCategoryName(String categoryName);

    PageDto<Provider> findPage(int pageNumber, int pageSize, String categoryName, String direction, String sortBy);
}

