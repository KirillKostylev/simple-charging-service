package com.netcracker.backend.service;

import com.netcracker.backend.entity.BillingAccount;
import com.netcracker.backend.entity.UserAccount;

public interface BillingAccountService extends Service<BillingAccount, Long> {
    BillingAccount addBillingAccountToUser(UserAccount userAccount);

    Iterable<BillingAccount> findBillingAccountsFromUser(Long id);

}
