package com.netcracker.backend.service.impl;

import com.netcracker.backend.dto.PageDto;
import com.netcracker.backend.entity.Provider;
import com.netcracker.backend.repository.ProviderRepository;
import com.netcracker.backend.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ProviderServiceImpl implements ProviderService {
    @Autowired
    private ProviderRepository providerRepository;


    @Override
    public Provider create(Provider provider) {
        return providerRepository.save(provider);
    }

    @Override
    public Optional<Provider> findById(Long id) {
        return providerRepository.findById(id);
    }

    @Override
    public void deleteById(Long id) {
        providerRepository.deleteById(id);
    }

    @Override
    public Provider update(Provider provider) {
        return providerRepository.save(provider);
    }

    @Override
    public Iterable<Provider> findAll() {
        return providerRepository.findAll();
    }

    @Override
    public PageDto<Provider> findPage(int pageNumber, int pageSize, String categoryName, String direction, String sortBy) {
        PageRequest pageRequest;
        if (direction != null && sortBy != null) {
            Sort.Direction tempDirection;
            if (direction.equalsIgnoreCase("Asc")) {
                tempDirection = Sort.Direction.ASC;
            } else {
                tempDirection = Sort.Direction.DESC;
            }
            pageRequest = PageRequest.of(pageNumber, pageSize, tempDirection, sortBy);
        } else {
            pageRequest = PageRequest.of(pageNumber, pageSize, Sort.Direction.DESC, "id");
        }

        Page<Provider> page;
        if (categoryName != null) {
            page = providerRepository.findPageByCategoryName(categoryName, pageRequest);
        } else {
            page = providerRepository.findAll(pageRequest);
        }
        return new PageDto<>(page);
    }


    @Override
    public Iterable<Provider> findAllByCategoryName(String categoryName) {
        return providerRepository.findAllByCategoryName(categoryName);
    }
}
