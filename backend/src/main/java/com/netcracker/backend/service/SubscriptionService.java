package com.netcracker.backend.service;

import com.netcracker.backend.entity.BillingAccount;
import com.netcracker.backend.entity.Subscription;
import com.netcracker.backend.entity.SubscriptionStatus;

public interface SubscriptionService extends Service<Subscription, Long> {
    Iterable<Subscription> findSubscriptionsFromUser(Long id);

    void changeSubscriptionStatus(Subscription subscription, SubscriptionStatus status);


}
