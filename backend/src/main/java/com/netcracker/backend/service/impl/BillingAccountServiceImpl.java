package com.netcracker.backend.service.impl;

import com.netcracker.backend.entity.BillingAccount;
import com.netcracker.backend.entity.UserAccount;
import com.netcracker.backend.repository.BillingAccountRepository;
import com.netcracker.backend.service.BillingAccountService;
import com.netcracker.backend.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public class BillingAccountServiceImpl implements BillingAccountService {
    @Autowired
    private BillingAccountRepository billingAccountRepository;

    @Autowired
    private SubscriptionService subscriptionService;

    @Override
    public Optional<BillingAccount> findById(Long id) {
        return billingAccountRepository.findById(id);
    }

    @Override
    public Iterable<BillingAccount> findAll() {
        return billingAccountRepository.findAll();
    }

    @Override
    public BillingAccount create(BillingAccount billingAccount) {
        return billingAccountRepository.save(billingAccount);
    }

    @Override
    public BillingAccount addBillingAccountToUser(UserAccount userAccount) {
        BillingAccount billingAccount = new BillingAccount(userAccount);
        return billingAccountRepository.save(billingAccount);
    }

    @Override
    public void deleteById(Long id) {
        billingAccountRepository.deleteById(id);
    }

    @Override
    public BillingAccount update(BillingAccount billingAccount) {
        return billingAccountRepository.save(billingAccount);
    }

    @Override
    public Iterable<BillingAccount> findBillingAccountsFromUser(Long id) {
        Collection<BillingAccount> allByUserAccountId = billingAccountRepository.findAllByUserAccountId(id);
        allByUserAccountId.forEach(ba -> ba.setUserAccount(null));
        return allByUserAccountId;
    }
}
