package com.netcracker.backend.service;

import com.netcracker.backend.entity.UserAccount;
import com.netcracker.backend.service.exception.EntryNotFoundException;

import java.util.List;

public interface UserAccountService extends Service<UserAccount, Long> {

    List<UserAccount> findAll(String login);

    UserAccount findByLogin(String login) throws EntryNotFoundException;

    Integer countSubscribesByUserId(Long id);
}
