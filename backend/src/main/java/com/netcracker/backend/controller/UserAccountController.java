package com.netcracker.backend.controller;

import com.netcracker.backend.entity.UserAccount;
import com.netcracker.backend.service.UserAccountService;
import com.netcracker.backend.service.exception.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/users")
public class UserAccountController {

    @Autowired
    private UserAccountService userAccountService;


    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<UserAccount> create(@RequestBody UserAccount userAccount) {
        UserAccount user = userAccountService.create(userAccount);
        return ResponseEntity.ok(user);
    }

    @DeleteMapping("/{id}")
    public void delete(@PathVariable("id") Long id) {
        userAccountService.deleteById(id);
    }

    @GetMapping
    public Iterable<UserAccount> findAllUserAccounts(@RequestParam(name = "login", required = false) String login) {
        return userAccountService.findAll(login);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserAccount> getUserAccountsById(@PathVariable("id") Long id) {
        Optional<UserAccount> userAccount = userAccountService.findById(id);
        return ResponseEntity.of(userAccount);
    }

    @PutMapping
    public UserAccount update(@RequestBody UserAccount userAccount) {
        return userAccountService.update(userAccount);
    }

    @GetMapping("/login/{login}")
    public ResponseEntity<UserAccount> checkingUserExistByLogin(@PathVariable("login") String login) throws EntryNotFoundException {
        UserAccount user = userAccountService.findByLogin(login);
        return ResponseEntity.ok(user);
    }

    @GetMapping("/{id}/subscriptions-count")
    public ResponseEntity<Integer> getSubscriptionsCountById(@PathVariable("id") Long id){
        Integer i = userAccountService.countSubscribesByUserId(id);
        return ResponseEntity.ok(i);
    }
}
