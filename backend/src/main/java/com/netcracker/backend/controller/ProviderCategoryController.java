package com.netcracker.backend.controller;

import com.netcracker.backend.entity.ProviderCategory;
import com.netcracker.backend.service.ProviderCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/service-categories")
public class ProviderCategoryController {
    @Autowired
    private ProviderCategoryService providerCategoryService;

    @GetMapping
    public ResponseEntity<Iterable<ProviderCategory>> getAllCategories() {
        Iterable<ProviderCategory> all = providerCategoryService.findAll();
        return ResponseEntity.ok(all);
    }

    @PostMapping
    public ResponseEntity<ProviderCategory> addNewCategory(@RequestBody ProviderCategory providerCategory) {
        providerCategory = providerCategoryService.create(providerCategory);
        return new ResponseEntity<>(providerCategory, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public void deleteCategory(@PathVariable Long id) {
        providerCategoryService.deleteById(id);
    }

    @PutMapping
    public ResponseEntity<ProviderCategory> update(@RequestBody ProviderCategory providerCategory) {
        ProviderCategory update = providerCategoryService.update(providerCategory);
        return ResponseEntity.ok(update);
    }
}
