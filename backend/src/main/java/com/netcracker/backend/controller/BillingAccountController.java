package com.netcracker.backend.controller;

import com.netcracker.backend.entity.BillingAccount;
import com.netcracker.backend.entity.UserAccount;
import com.netcracker.backend.service.BillingAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping(value = "/api/billing-accounts")
public class BillingAccountController {
    @Autowired
    private BillingAccountService billingAccountService;

    @PostMapping
    public ResponseEntity<BillingAccount> addBillingAccountToUser(@RequestBody UserAccount userAccount) {
        BillingAccount billingAccount = billingAccountService.addBillingAccountToUser(userAccount);
        return new ResponseEntity<>(billingAccount, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Iterable<BillingAccount>> getAll() {
        Iterable<BillingAccount> all = billingAccountService.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping("/{id}")
    public ResponseEntity<BillingAccount> getById(@PathVariable(name = "id") Long id) {
        Optional<BillingAccount> resultBillingAccount = billingAccountService.findById(id);
        return ResponseEntity.of(resultBillingAccount);
    }

    @PutMapping
    public ResponseEntity<BillingAccount> update(@RequestBody BillingAccount billingAccount) {
        BillingAccount update = billingAccountService.update(billingAccount);
        return ResponseEntity.ok(update);
    }

    @DeleteMapping("/{id}")
    public void deleteBillingAccount(@PathVariable("id") Long id) {
        billingAccountService.deleteById(id);
    }

    @GetMapping("/users/{id}")
    public Iterable<BillingAccount> getBillingAccountFromUser(@PathVariable("id") Long id) {
        return billingAccountService.findBillingAccountsFromUser(id);
    }
}
