package com.netcracker.backend.controller;

import com.netcracker.backend.dto.PageDto;
import com.netcracker.backend.entity.Provider;
import com.netcracker.backend.service.ProviderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("api/services")
public class ProviderController {
    @Autowired
    private ProviderService providerService;

    @GetMapping("/pages")
    public ResponseEntity<PageDto<Provider>> findPages(
            @RequestParam("pageNumber") int pageNumber,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "categoryName", required = false) String categoryName,
            @RequestParam(value = "direction", required = false) String direction,
            @RequestParam(value = "sortBy", required = false) String sortBy) {
        PageDto<Provider> pageDto = providerService.findPage(pageNumber, pageSize, categoryName, direction, sortBy);
        return ResponseEntity.ok(pageDto);
    }

    @GetMapping()
    public ResponseEntity<Iterable<Provider>> findAll() {
        Iterable<Provider> providers = providerService.findAll();
        return ResponseEntity.ok(providers);
    }

    @GetMapping("/category/{name}")
    public ResponseEntity<Iterable<Provider>> findByCategory(@PathVariable String name) {
        return ResponseEntity.ok(providerService.findAllByCategoryName(name));
    }

    @PostMapping
    public ResponseEntity<Provider> createProvider(@RequestBody Provider provider) {
        provider = providerService.create(provider);
        return new ResponseEntity<>(provider, HttpStatus.CREATED);
    }

    @PutMapping
    public ResponseEntity<Provider> updateProvider(@RequestBody Provider provider) {
        provider = providerService.update(provider);
        return ResponseEntity.ok(provider);
    }

    @DeleteMapping("/{id}")
    public void deleteProviderById(@PathVariable("id") Long id) {
        providerService.deleteById(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Provider> findProviderById(@PathVariable("id") Long id) {
        return ResponseEntity.of(providerService.findById(id));
    }
}
