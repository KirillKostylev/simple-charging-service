package com.netcracker.backend.controller;

import com.netcracker.backend.entity.Subscription;
import com.netcracker.backend.service.SubscriptionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api/subscriptions")
public class SubscriptionController {
//    private static final Logger LOGGER = LoggerFactory.getLogger(SubscriptionController.class);

    @Autowired
    private SubscriptionService subscriptionService;

    @PostMapping
    public ResponseEntity<Subscription> create(@RequestBody Subscription subscription) {
        Subscription body = subscriptionService.create(subscription);
        return new ResponseEntity<>(body, HttpStatus.CREATED);
    }

    @GetMapping
    public ResponseEntity<Iterable<Subscription>> getAllSubscriptions() {
        Iterable<Subscription> all = subscriptionService.findAll();
        return ResponseEntity.ok(all);
    }

    @GetMapping("/{id}")
    public ResponseEntity<Subscription> getSubscriptionById(@PathVariable("id") Long id) {
        Optional<Subscription> subscription = subscriptionService.findById(id);
        return ResponseEntity.of(subscription);
    }

    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable("id") Long id) {
        subscriptionService.deleteById(id);
    }

    @PutMapping
    public ResponseEntity<Subscription> update(@RequestBody Subscription subscription) {
        Subscription update = subscriptionService.update(subscription);
        return ResponseEntity.ok(update);
    }

    @GetMapping("/users/{id}")
    public Iterable<Subscription> getSubscriptionsFromUser(@PathVariable("id") Long id) {
        return subscriptionService.findSubscriptionsFromUser(id);
    }

}
