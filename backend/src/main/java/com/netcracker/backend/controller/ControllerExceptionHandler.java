package com.netcracker.backend.controller;

import com.netcracker.backend.service.exception.EntryNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntryNotFoundException.class)
    protected ResponseEntity<OutputException> handleEntryNotFoundException(EntryNotFoundException e) {
        return new ResponseEntity<>(new OutputException(e.getMessage()), HttpStatus.NOT_FOUND);
    }


    @Data
    @AllArgsConstructor
    private static class OutputException {
        private String message;
    }
}
