package com.netcracker.backend.repository;

import com.netcracker.backend.entity.Provider;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;


public interface ProviderRepository extends PagingAndSortingRepository<Provider, Long> {
    Iterable<Provider> findAllByCategoryName(String categoryName);

    Page<Provider> findPageByCategoryName(String categoryName, Pageable pageable);

    Page<Provider> findAll(Pageable pageable);
}
