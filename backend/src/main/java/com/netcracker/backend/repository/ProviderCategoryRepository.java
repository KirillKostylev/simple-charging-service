package com.netcracker.backend.repository;

import com.netcracker.backend.entity.ProviderCategory;
import org.springframework.data.repository.CrudRepository;

public interface ProviderCategoryRepository extends CrudRepository<ProviderCategory, Long> {
}
