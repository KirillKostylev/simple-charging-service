package com.netcracker.backend.repository;

import com.netcracker.backend.entity.Subscription;
import org.springframework.data.repository.CrudRepository;

public interface SubscriptionRepository extends CrudRepository<Subscription, Long> {
    Iterable<Subscription> findAllByBillingAccount_UserAccountIdOrderByStatusIdAsc(Long id);
    Iterable<Subscription> findAllByProviderId(Long id);
    Integer countSubscriptionByBillingAccount_UserAccountId(Long id);
}
