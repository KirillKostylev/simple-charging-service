package com.netcracker.backend.repository;

import com.netcracker.backend.entity.BillingAccount;
import org.springframework.data.repository.CrudRepository;

import java.util.Collection;

public interface BillingAccountRepository extends CrudRepository<BillingAccount, Long> {
    Collection<BillingAccount> findAllByUserAccountId(Long id);
}
