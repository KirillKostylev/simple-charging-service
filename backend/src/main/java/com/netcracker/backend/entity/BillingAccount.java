package com.netcracker.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import static com.netcracker.backend.BackendConstants.DEFAULT_BALANCE;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "billing_account")
public class BillingAccount {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private double balance = DEFAULT_BALANCE;

    @ManyToOne
    @JoinColumn(name = "user_account_id")
    private UserAccount userAccount;

    @OneToMany(mappedBy = "billingAccount", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Subscription> subscriptions = new HashSet<>();

    public BillingAccount(UserAccount userAccount) {
        this.userAccount = userAccount;
    }

}
