package com.netcracker.backend.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "service")
public class Provider {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    private String name;

    private String description;

    private double price;

    @Lob
    private byte[] photo = null;

    @ManyToOne
    @JoinColumn(name = "service_category_id")
    private ProviderCategory category;

    @OneToMany(mappedBy = "provider", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Subscription> subscriptions = new HashSet<>();

    @ManyToOne
    @JoinColumn(name = "owner_billing_account_id")
    private BillingAccount ownerBillingAccount;

}
