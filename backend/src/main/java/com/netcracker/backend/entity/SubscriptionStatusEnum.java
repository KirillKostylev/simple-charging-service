package com.netcracker.backend.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum SubscriptionStatusEnum {
    NOT_ENOUGH_MONEY(new SubscriptionStatus(1L, "Not enough money")),
    ACTIVE(new SubscriptionStatus(2L, "Active")),
    FINISHED(new SubscriptionStatus(3L, "Finished"));

    private SubscriptionStatus status;
}
