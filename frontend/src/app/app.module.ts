import {BrowserModule} from '@angular/platform-browser';
import {APP_INITIALIZER, NgModule, NO_ERRORS_SCHEMA} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {HeaderComponent} from './modules/components/layout/header/header.component';
import {ProviderComponent} from './modules/components/provider/provider/provider.component';
import {NotFoundComponent} from './modules/components/layout/not-found/not-found.component';
import {HomeComponent} from './modules/components/layout/home/home.component';
import {ProviderCategoriesComponent} from './modules/components/provider-category/provider-category/provider-categories.component';
import {SignInComponent} from './modules/components/user/sign-in/sign-in.component';
import {SignUpComponent} from './modules/components/user/sign-up/sign-up.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {ProviderCategoryService} from './service/provider-category.service';
import {ViewAllProvidersComponent} from './modules/components/provider/view-all-providers/view-all-providers.component';
import {CategoryManagementComponent} from './modules/components/provider-category/category-management/category-management.component';
import {AddProviderComponent} from './modules/components/provider/add-provider/add-provider.component';
import {ViewAllUsersComponent} from './modules/components/user/view-all-users/view-all-users.component';
import {EditProviderComponent} from './modules/components/provider/edit-provider/edit-provider.component';
import {AllBillingAccountsComponent} from './modules/components/billing-account/all-billing-accounts/all-billing-accounts.component';
import {IncreaseBillComponent} from './modules/components/billing-account/increase-bill/increase-bill.component';
import {MySubscriptionsComponent} from './modules/components/subscriptions/my-subscriptions/my-subscriptions.component';
import {SubscribeComponent} from './modules/components/subscriptions/subscribe/subscribe.component';
import {AuthenticationInterceptor} from './interceptor/authentication.interceptor';
import {MDBBootstrapModule, MDBModalRef} from 'angular-bootstrap-md';
import {TokenService} from './service/token.service';

const appRoutes: Routes = [
  {path: '', component: HomeComponent},
  {path: 'billing-accounts', component: AllBillingAccountsComponent},
  {path: 'subscriptions', component: MySubscriptionsComponent},
  {path: 'services', component: ViewAllProvidersComponent},
  {path: 'users', component: ViewAllUsersComponent},
  {path: 'services/add', component: AddProviderComponent},
  {path: 'services/:id', component: ProviderComponent},
  {path: 'services/**', component: NotFoundComponent},
  {path: '**', component: NotFoundComponent},
  {path: 'not-found', component: NotFoundComponent},
];

const appConfig = (tokenService: TokenService) => {
  return () => {
    return tokenService.getUserDetails();
  };
};

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ProviderComponent,
    NotFoundComponent,
    HomeComponent,
    ProviderCategoriesComponent,
    SignInComponent,
    SignUpComponent,
    ViewAllProvidersComponent,
    CategoryManagementComponent,
    AddProviderComponent,
    ViewAllUsersComponent,
    EditProviderComponent,
    AllBillingAccountsComponent,
    IncreaseBillComponent,
    MySubscriptionsComponent,
    SubscribeComponent,

  ],
  imports: [
    MDBBootstrapModule.forRoot(),
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    RouterModule.forRoot(appRoutes),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  entryComponents: [
    SignInComponent,
    SignUpComponent,
    CategoryManagementComponent,
    SubscribeComponent,
    EditProviderComponent,
    IncreaseBillComponent
  ],
  schemas: [
    NO_ERRORS_SCHEMA
  ],
  providers: [
    ProviderCategoryService,
    MDBModalRef,
    {
      provide: APP_INITIALIZER,
      useFactory: appConfig,
      multi: true,
      deps: [TokenService]
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthenticationInterceptor,
      multi: true
    }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
