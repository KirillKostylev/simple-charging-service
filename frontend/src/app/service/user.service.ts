import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {User} from '../entities/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private url = '/api/fapi/users/';

  constructor(private http: HttpClient) {
  }

  getAll(): Observable<User[]> {
    return this.http.get<User[]>(this.url);
  }


  create(user: User) {
    return this.http.post<User>(this.url, user);
  }


  checkingUserExistByLogin(login: string): Observable<User> {
    return this.http.get<User>(this.url + 'login/' + login);
  }
}
