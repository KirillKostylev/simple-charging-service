import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {ProviderCategory} from '../entities/provider-category';
import {Observable} from 'rxjs';
import {tap} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class ProviderCategoryService {
  private url = '/api/fapi/service-categories/';
  public categories: ProviderCategory[];

  constructor(private http: HttpClient) {
  }

  getAllCategories(): Observable<ProviderCategory[]> {
    return this.http.get<ProviderCategory[]>(this.url)
      .pipe(tap(categories => this.categories = categories));
  }

  createCategory(providerCategory: ProviderCategory): Observable<ProviderCategory> {
    return this.http.post<ProviderCategory>(this.url, providerCategory)
      .pipe(tap(category => this.categories.push(category)));
  }

  deleteById(id: number): Observable<any> {
    return this.http.delete(this.url + id).pipe(tap(_ => {
      // tslint:disable-next-line:triple-equals
      const i = this.categories.findIndex(category => category.id == id);
      this.categories.splice(i, 1);
    }));
  }

  findById(id: number): ProviderCategory {
    // tslint:disable-next-line:triple-equals
    return this.categories.find(category => category.id == id);
  }

  checkCategory(categoryName: string) {
    // tslint:disable-next-line:triple-equals
    return this.categories.find(a => a.name == categoryName);
  }

}
