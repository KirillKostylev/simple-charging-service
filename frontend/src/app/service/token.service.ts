import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {SecurityConstants} from '../entities/AuthToken';

@Injectable({
  providedIn: 'root'
})
export class TokenService {
  private tokenUrl = '/api/fapi/token';
  public userDetails;

  constructor(private http: HttpClient) {
  }

  generateToken(userLogin: { login: string, password: string }) {
    return this.http.post(this.tokenUrl + '/generate-token', userLogin);
  }

  getToken(): string {
    return localStorage.getItem(SecurityConstants.AUTHORIZATION);
  }

  getUserDetails() {
    return this.http.get(this.tokenUrl + '/details').toPromise()
      .then((res: any) => {
        if (!res.message) {
          this.userDetails = res;
        }
      }).then(_ => console.log(this.userDetails));
  }

  logOut() {
    localStorage.removeItem(SecurityConstants.AUTHORIZATION);
    this.userDetails = null;
  }
}
