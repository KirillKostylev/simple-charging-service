import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {ProviderModel} from '../entities/provider';

@Injectable({
  providedIn: 'root'
})
export class ProviderService {
  private url = '/api/fapi/services/';
  // public allProviders: ProviderModel[];

  constructor(private http: HttpClient) {
  }

  getById(id: number): Observable<ProviderModel> {
    return this.http.get<ProviderModel>(this.url + id);
  }

  createProvider(formData: FormData): Observable<any> {
    return this.http.post(this.url, formData);
  }


  deleteProvider(id: number): Observable<any> {
    return this.http.delete(this.url + id);
  }

  updateProvider(formData: FormData): Observable<any> {
    return this.http.put(this.url, formData);
  }

  getPage(params: HttpParams) {
    return this.http.get(this.url + 'pages', {params});
  }

  getPageByCategoryName(name: string, pageNumber: number, pageSize: number) {
    return this.http.get(this.url + 'pages/category/' + name + '/?pageNumber=' + pageNumber +
      '&pageSize=' + pageSize);
  }
}
