import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Subscription} from '../entities/subscription';

@Injectable({
  providedIn: 'root'
})
export class SubscriptionService {
  private url = '/api/fapi/subscriptions/';

  constructor(private http: HttpClient) {
  }

  delete(id: number): Observable<any> {
    return this.http.delete(this.url + id);
  }

  create(subscription: Subscription) {
    return this.http.post(this.url, subscription);
  }

  getSubscriptionsByUserId(id: number): Observable<Subscription[]> {
    return this.http.get<Subscription[]>(this.url + 'users/' + id);
  }

}
