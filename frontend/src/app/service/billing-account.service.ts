import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BillingAccount} from '../entities/billing-account';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class BillingAccountService {
  private url = '/api/fapi/billing-accounts/';

  constructor(private http: HttpClient) {
  }

  createBa(user: { id: number }) {
    return this.http.post(this.url, user);
  }

  increaseBalance(billingAccount: BillingAccount) {
    return this.http.put(this.url, billingAccount);
  }

  deleteBA(id: number) {
    return this.http.delete(this.url + id);
  }

  getBillingAccountsByUserId(id: number): Observable<BillingAccount[]> {
    return this.http.get<BillingAccount[]>(this.url + 'users/' + id);
  }

}
