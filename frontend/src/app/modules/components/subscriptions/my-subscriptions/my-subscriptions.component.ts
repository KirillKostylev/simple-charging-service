import {Component, OnInit} from '@angular/core';
import {SubscriptionService} from '../../../../service/subscription.service';
import {TokenService} from '../../../../service/token.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-my-subscriptions',
  templateUrl: './my-subscriptions.component.html',
  styleUrls: ['./my-subscriptions.component.css']
})
export class MySubscriptionsComponent implements OnInit {
  private interval;
  private subscribes: Subscription[] = [];
  public subscriptions;
  public loading = false;

  constructor(private subscriptionService: SubscriptionService,
              private tokenService: TokenService) {
  }


  ngOnInit(): void {
    this.loading = true;
    this.updateSubscriptions();
    this.interval = setInterval(() => {
      this.updateSubscriptions();
    }, 3000);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    clearInterval(this.interval);
    this.subscribes.forEach(s => s.unsubscribe());
  }

  private updateSubscriptions() {
    this.subscribes.push(this.subscriptionService.getSubscriptionsByUserId(this.user?.id)
      .subscribe(res => {
        this.subscriptions = res;
        this.loading = false;
      }));
  }

  delete(id: number) {
    this.loading = true;
    this.subscribes.push(this.subscriptionService.delete(id).subscribe(res => {
      this.updateSubscriptions();
      this.loading = false;
    }));
  }

  get user() {
    return this.tokenService.userDetails;
  }
}
