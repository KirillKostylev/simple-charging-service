import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {BillingAccount} from '../../../../entities/billing-account';
import {Subscription} from '../../../../entities/subscription';
import {SubscriptionService} from '../../../../service/subscription.service';
import {DatePipe} from '@angular/common';
import {MDBModalRef} from 'angular-bootstrap-md';
import {ProviderModel} from '../../../../entities/provider';
import {Router} from '@angular/router';


enum time {
  hInDay = 24,
  minInH = 60,
  secInMin = 60,
  mlsInSec = 1000
}

@Component({
  selector: 'app-subscribe',
  templateUrl: './subscribe.component.html',
  styleUrls: ['./subscribe.component.css'],
  providers: [DatePipe]
})
export class SubscribeComponent implements OnInit {
  private currentDate: Date;
  public billingAccounts: BillingAccount[];
  public provider: ProviderModel;
  public currentDateString: string;

  endDate = new FormControl('', [
    Validators.required
  ]);

  billingAccountId = new FormControl(null, Validators.required);

  constructor(private subscriptionService: SubscriptionService,
              private datePipe: DatePipe,
              private router: Router,
              public mdbModalRef: MDBModalRef) {
  }

  ngOnInit(): void {
    this.currentDate =
      new Date(new Date().getTime() + (time.mlsInSec * time.secInMin * time.minInH * time.hInDay));
    this.currentDateString = this.datePipe.transform(this.currentDate, 'yyyy-MM-dd');
  }


  subscribe() {
    const subscription = new Subscription();
    subscription.endTime = new Date(new Date(
      Date.parse(this.endDate.value)).getTime() - 3 * time.minInH * time.secInMin * time.mlsInSec);

    const currentHours = this.currentDate.getHours();
    const currentMin = this.currentDate.getMinutes();
    const currentMls = (currentHours * time.minInH + currentMin) * time.secInMin * time.mlsInSec;

    subscription.endTime = new Date(subscription.endTime.getTime() + currentMls);
    subscription.billingAccountId = this.billingAccountId.value;
    subscription.providerId = this.provider.id;
    this.subscriptionService.create(subscription).subscribe(res => {
      this.router.navigate(['../subscriptions']);
      this.mdbModalRef.hide();
    });
  }
}
