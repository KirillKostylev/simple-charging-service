import {Component, OnInit, Renderer2} from '@angular/core';

import {SignUpComponent} from '../../user/sign-up/sign-up.component';
import {CategoryManagementComponent} from '../../provider-category/category-management/category-management.component';
import {MDBModalRef, MDBModalService} from 'angular-bootstrap-md';
import {ProviderCategoryService} from '../../../../service/provider-category.service';
import {TokenService} from '../../../../service/token.service';
import {SignInComponent} from '../../user/sign-in/sign-in.component';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  public modalRef: MDBModalRef;

  constructor(private providerCategoryService: ProviderCategoryService,
              private tokenService: TokenService,
              private modalService: MDBModalService,
              private renderer: Renderer2) {
  }

  ngOnInit(): void {
    this.providerCategoryService.getAllCategories().subscribe();

  }


  logOut() {
    this.tokenService.logOut();
  }

  showSignInModal() {
    this.modalRef = this.modalService.show(SignInComponent);
  }

  showSignUpModal() {
    this.modalRef = this.modalService.show(SignUpComponent);
    this.renderer.setStyle(document.querySelector('mdb-modal-container'), 'overflow-y', 'auto');
  }

  showCategoryManagementModal() {
    this.modalRef = this.modalService.show(CategoryManagementComponent);
  }

  get categories() {
    return this.providerCategoryService.categories;
  }

  get userRole() {
    return this.tokenService.userDetails?.authorities[0]?.authority;
  }

  get user() {
    return this.tokenService?.userDetails;
  }
}
