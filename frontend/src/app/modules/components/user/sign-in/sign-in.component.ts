import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {TokenService} from '../../../../service/token.service';
import {AuthToken, SecurityConstants} from '../../../../entities/AuthToken';
import {MDBModalRef} from 'angular-bootstrap-md';
import {Subscription} from 'rxjs';
import {UserConstants} from '../../../../entities/user';

@Component({
  selector: 'app-sign-in',
  templateUrl: './sign-in.component.html',
  styleUrls: ['./sign-in.component.css'],
})
export class SignInComponent implements OnInit {
  private subscribes: Subscription[] = [];
  public error: string;

  form: FormGroup = new FormGroup({
    login: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Zа-яА-Я\'_0-9]{4,40}$')
    ]),

    password: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Zа-яА-Я\'_0-9]{4,40}$')
    ]),
  });

  constructor(private tokenService: TokenService,
              public mdbModalRef: MDBModalRef) {
  }

  ngOnInit(): void {
  }

  signIn() {
    const user = {
      login: this.form.get(UserConstants.LOGIN).value,
      password: this.form.get(UserConstants.PASSWORD).value,
    };
    this.subscribes.push(this.tokenService.generateToken(user).subscribe((res: AuthToken) => {
      localStorage.setItem(SecurityConstants.AUTHORIZATION, res.value);
      this.tokenService.getUserDetails();
      this.mdbModalRef.hide();
    }, e => {
      this.error = e.error.message;
    }));
    this.form.reset();
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscribes.forEach(s => s.unsubscribe());
  }
}

