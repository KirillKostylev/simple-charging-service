import {Component, OnInit} from '@angular/core';
import {UserService} from '../../../../service/user.service';
import {User} from '../../../../entities/user';
import {BillingAccount} from '../../../../entities/billing-account';
import {BillingAccountService} from '../../../../service/billing-account.service';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-view-all-users',
  templateUrl: './view-all-users.component.html',
  styleUrls: ['./view-all-users.component.css']
})
export class ViewAllUsersComponent implements OnInit {
  private subscribes: Subscription[] = [];
  public users: User[];
  public baListFromUser: BillingAccount[];
  public loading = false;

  constructor(private userService: UserService,
              private baService: BillingAccountService) {
  }

  ngOnInit(): void {
    this.loading = true;
    this.subscribes.push(this.userService.getAll().subscribe(value => {
      this.users = value;
      this.loading = false;
    }));
  }

  getBillingAccountsByUserId(id: number) {
    this.subscribes.push(this.baService.getBillingAccountsByUserId(id)
      .subscribe(value => this.baListFromUser = value));
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscribes.forEach(s => s.unsubscribe());
  }
}
