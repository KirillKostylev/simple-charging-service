import {Component, OnInit} from '@angular/core';
import {AbstractControl, AsyncValidatorFn, FormControl, FormGroup, ValidationErrors, Validators} from '@angular/forms';
import {User, UserConstants} from '../../../../entities/user';
import {Role} from '../../../../entities/role';
import {UserService} from '../../../../service/user.service';
import {debounceTime, distinctUntilChanged, first, map, switchMap} from 'rxjs/operators';
import {MDBModalRef} from 'angular-bootstrap-md';
import {AuthToken, SecurityConstants} from '../../../../entities/AuthToken';
import {TokenService} from '../../../../service/token.service';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-sign-up',
  templateUrl: './sign-up.component.html',
  styleUrls: ['./sign-up.component.css']
})
export class SignUpComponent implements OnInit {
  private subscribes: Subscription[] = [];

  form: FormGroup = new FormGroup({
    isClient: new FormControl(true, []),

    firstName: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Zа-яА-Я\']{2,40}$')
    ]),

    lastName: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Zа-яА-Я\']{2,40}$')
    ]),

    login: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Zа-яА-Я\'_0-9]{4,40}$'
      )], [
      this.loginValidator()
    ]),

    password: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Zа-яА-Я\'_0-9]{4,40}$')
    ]),

    confirmPassword: new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Zа-яА-Я\'_0-9]{4,40}$'),
      this.matchValues('password')
    ])
  });

  constructor(private userService: UserService,
              public mdbModalRef: MDBModalRef,
              private tokenService: TokenService) {

  }

  ngOnInit(): void {
    this.subscribes.push(this.form.controls.password.valueChanges.subscribe(() => {
      this.form.controls.confirmPassword.updateValueAndValidity();
    }));
  }


  register() {
    const user = new User();
    user.login = this.form.controls[UserConstants.LOGIN].value;
    user.firstName = this.form.controls[UserConstants.FIRST_NAME].value;
    user.lastName = this.form.controls[UserConstants.LAST_NAME].value;
    user.password = this.form.controls[UserConstants.PASSWORD].value;
    user.role = this.form.controls['isClient'].value ? this.getClientRole : this.getProviderRole;

    this.subscribes.push(this.userService.create(user).subscribe(_ => {
      this.mdbModalRef.hide();

      this.subscribes.push(this.tokenService.generateToken(user).subscribe((res: AuthToken) => {
        localStorage.setItem(SecurityConstants.AUTHORIZATION, res.value);
        this.tokenService.getUserDetails();
        this.mdbModalRef.hide();
      }));

    }));
  }

  private loginValidator(): AsyncValidatorFn {
    return control => control.valueChanges
      .pipe(
        debounceTime(500),
        distinctUntilChanged(),
        switchMap((val: string) => this.userService.checkingUserExistByLogin(val)),
        map((res: User) => (res.login != null ? {loginExist: true} : null)),
        first()
      );
  }


  public matchValues(passwordControlName: string): (AbstractControl) => ValidationErrors | null {
    return (control: AbstractControl): ValidationErrors | null => {
      return !!control.parent && !!control.parent.value &&
      control.value === control.parent.controls[passwordControlName].value ? null : {isMatching: false};
    };
  }

  get login() {
    return this.form.get(UserConstants.LOGIN);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscribes.forEach(s => s.unsubscribe());
  }

  get getClientRole(){
    var role = new Role();
    role.id = 2;
    role.name = 'CLIENT'
    return role;
  }

  get getProviderRole(){
      var role = new Role();
      role.id = 3;
      role.name = 'PROVIDER'
      return role;
    }
}

