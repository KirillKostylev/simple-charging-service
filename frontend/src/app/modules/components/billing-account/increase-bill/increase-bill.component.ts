import {Component, OnInit} from '@angular/core';
import {FormControl, Validators} from '@angular/forms';
import {BillingAccount} from '../../../../entities/billing-account';
import {BillingAccountService} from '../../../../service/billing-account.service';
import {MDBModalRef} from 'angular-bootstrap-md';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-increase-bill',
  templateUrl: './increase-bill.component.html',
  styleUrls: ['./increase-bill.component.css']
})
export class IncreaseBillComponent implements OnInit {
  private subscribes: Subscription[] = [];
  public id: number;

  count: FormControl = new FormControl('', [
    Validators.required,
    Validators.pattern('^[0-9]+(\\.[0-9]{1,2})?$'),
    Validators.min(0.1)
  ]);


  constructor(private baService: BillingAccountService,
              public mdbModalRef: MDBModalRef) {
  }

  ngOnInit(): void {
  }


  increase() {
    const body = new BillingAccount(this.id, this.count.value);
    this.subscribes.push(this.baService.increaseBalance(body).subscribe(res => this.mdbModalRef.hide()));
    this.count.reset();
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscribes.forEach(s => s.unsubscribe());
  }
}


