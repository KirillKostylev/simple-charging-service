import {Component, OnInit} from '@angular/core';
import {BillingAccount} from '../../../../entities/billing-account';
import {BillingAccountService} from '../../../../service/billing-account.service';
import {TokenService} from '../../../../service/token.service';
import {MDBModalRef, MDBModalService} from 'angular-bootstrap-md';
import {IncreaseBillComponent} from '../increase-bill/increase-bill.component';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-all-billing-accounts',
  templateUrl: './all-billing-accounts.component.html',
  styleUrls: ['./all-billing-accounts.component.css']
})


export class AllBillingAccountsComponent implements OnInit {
  private interval;
  private subscribes: Subscription[] = [];
  public loading = false;
  public modalRef: MDBModalRef;

  billingAccounts: BillingAccount[];

  constructor(private baService: BillingAccountService,
              private tokenService: TokenService,
              private modalService: MDBModalService) {
  }


  ngOnInit(): void {
    this.loading = true;
    this.updateBa();

    this.interval = setInterval(() => {
      this.updateBa();
    }, 3000);
  }


  private updateBa() {
    if (this.tokenService.userDetails?.id != null) {
      this.subscribes.push(this.baService.getBillingAccountsByUserId(this.user?.id).subscribe(res => {
        this.billingAccounts = res;
        this.loading = false;
      }));
    }
  }

  remove(id: number) {
    // tslint:disable-next-line:triple-equals
    const i = this.billingAccounts.findIndex(ba => ba.id == id);
    this.billingAccounts.splice(i, 1);
    this.subscribes.push(this.baService.deleteBA(id).subscribe());
  }

  addBA() {
    const user = {id: this.user.id};
    this.subscribes.push(this.baService.createBa(user).subscribe(newBA => {
      this.billingAccounts.push(newBA as BillingAccount);
    }));
  }

  showIncreaseBAModal(idBa: number) {
    const modalOptions = {
      data: {
        id: idBa
      }
    };
    this.modalRef = this.modalService.show(IncreaseBillComponent, modalOptions);
  }

  get user() {
    return this.tokenService.userDetails;
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy(): void {
    clearInterval(this.interval);
    this.subscribes.forEach(s => s.unsubscribe());
  }
}
