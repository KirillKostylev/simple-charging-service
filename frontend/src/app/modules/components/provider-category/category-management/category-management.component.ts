import {Component, OnInit} from '@angular/core';
import {ProviderCategory} from '../../../../entities/provider-category';
import {ProviderCategoryService} from '../../../../service/provider-category.service';
import {MDBModalRef} from 'angular-bootstrap-md';
import {AsyncValidatorFn, FormControl, Validators} from '@angular/forms';
import {debounceTime, distinctUntilChanged, first, map} from 'rxjs/operators';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-add-category',
  templateUrl: './category-management.component.html',
  styleUrls: ['./category-management.component.css']
})
export class CategoryManagementComponent implements OnInit {
  private subscribes: Subscription[] = [];
  public categoryId;
  public categoryName;

  name = new FormControl('', [
      Validators.required,
      Validators.pattern('^[a-zA-Zа-яА-Я0-9]{2,40}$')
    ],
    this.categoryValidator());


  constructor(private providerCategoryService: ProviderCategoryService,
              public mdbModalRef: MDBModalRef) {
  }

  ngOnInit(): void {
    this.subscribes.push(this.providerCategoryService.getAllCategories().subscribe());
  }

  addCategory() {
    const categoryName = this.name.value;
    this.name.reset();
    this.subscribes.push(this.providerCategoryService.createCategory(new ProviderCategory(categoryName)).subscribe());
  }

  deleteCategory(categoryId: number, name: string) {
    this.subscribes.push(this.providerCategoryService.deleteById(categoryId).subscribe());
  }

  getCategories() {
    return this.providerCategoryService.categories;
  }

  private categoryValidator(): AsyncValidatorFn {
    return control => control.valueChanges
      .pipe(
        debounceTime(100),
        distinctUntilChanged(),
        map((val: string) => this.providerCategoryService.checkCategory(val) != null ? {categoryExist: true} : null),
        first()
      );
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscribes.forEach(s => s.unsubscribe());
  }
}
