import {Component, OnInit} from '@angular/core';
import {ProviderCategory} from '../../../../entities/provider-category';
import {ProviderCategoryService} from '../../../../service/provider-category.service';

@Component({
  selector: 'app-service-categories',
  templateUrl: './provider-categories.component.html',
  styleUrls: ['./provider-categories.component.css']
})
export class ProviderCategoriesComponent implements OnInit {

  constructor(private providerCategoriesService: ProviderCategoryService) {
  }

  ngOnInit(): void {
    this.providerCategoriesService.getAllCategories();
  }

}
