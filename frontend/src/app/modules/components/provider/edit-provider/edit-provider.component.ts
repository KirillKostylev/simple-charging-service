import {Component, OnInit} from '@angular/core';
import {ProviderModel} from '../../../../entities/provider';
import {ProviderService} from '../../../../service/provider.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MDBModalRef} from 'angular-bootstrap-md';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-edit-provider',
  templateUrl: './edit-provider.component.html',
  styleUrls: ['./edit-provider.component.css']
})
export class EditProviderComponent implements OnInit {
  private subscribes: Subscription[] = [];
  public provider: ProviderModel;
  public selectedPhoto: File;
  public providerForm: FormGroup;

  constructor(private providerService: ProviderService,
              public mdbModalRef: MDBModalRef) {
  }

  ngOnInit(): void {
    this.providerForm = new FormGroup({

      name: new FormControl(this.provider.name, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50)
      ]),

      price: new FormControl(this.provider.price, [
        Validators.pattern('^[0-9]+(\\.[0-9]{1,2})?$'),
        Validators.min(0.1),
        Validators.max(99999999.99),
        Validators.required
      ]),

      description: new FormControl(this.provider.description, [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(1000)
      ]),

      photo: new FormControl(null)
    });
  }

  onFileSelected(event) {
    this.selectedPhoto = event.target.files[0];
    if (!(this.selectedPhoto.name.endsWith('.png') || this.selectedPhoto.name.endsWith('.jpg'))) {
      this.providerForm.controls.photo.setErrors({'incorrect photo': true});
      this.selectedPhoto = null;
    }
  }

  updateProvider() {
    const providerData = new FormData();
    if (this.selectedPhoto != null) {
      providerData.append('photo', this.selectedPhoto);
    }

    this.providerFillingFromFormGroup();
    providerData.append('provider', JSON.stringify(this.provider));
    console.log(this.provider);
    this.subscribes.push(this.providerService.updateProvider(providerData)
      .subscribe((res: ProviderModel) => {
        this.mdbModalRef.hide();
        this.provider = res;
      }));
  }

  providerFillingFromFormGroup() {
    this.provider.name = this.providerForm.get('name').value;
    this.provider.price = this.providerForm.get('price').value;
    this.provider.description = this.providerForm.get('description').value;
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscribes.forEach(s => s.unsubscribe());
  }
}
