import {Component, OnInit} from '@angular/core';
import {ProviderService} from '../../../../service/provider.service';
import {TokenService} from '../../../../service/token.service';
import {BillingAccountService} from '../../../../service/billing-account.service';
import {ProviderCategoryService} from '../../../../service/provider-category.service';
import {ProviderConstants, ProviderModel} from '../../../../entities/provider';
import {BillingAccount} from '../../../../entities/billing-account';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';


@Component({
  selector: 'app-add-service',
  templateUrl: './add-provider.component.html',
  styleUrls: ['./add-provider.component.css']
})
export class AddProviderComponent implements OnInit {
  public selectedPhoto: File;
  private subscribes: Subscription[] = [];
  public id: number;
  public billingAccounts: BillingAccount[];


  providerForm: FormGroup;

  constructor(private providerService: ProviderService,
              private categoryService: ProviderCategoryService,
              private billingAccountService: BillingAccountService,
              private tokenService: TokenService,
              private router: Router) {
  }

  ngOnInit(): void {
    this.categoryService.getAllCategories().subscribe();

    this.subscribes.push(this.billingAccountService.getBillingAccountsByUserId(this.user?.id)
            .subscribe(res => this.billingAccounts = res));

    this.providerForm = new FormGroup({

      name: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50)
      ]),

      price: new FormControl('0.00', [
        Validators.pattern('^[0-9]+(\\.[0-9]{1,2})?$'),
        Validators.min(0.1),
        Validators.max(99999999.99),
        Validators.required
      ]),

      description: new FormControl('', [
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(1000)
      ]),

      categoryId: new FormControl(null, [
        Validators.required
      ]),

      ownerBillingAccountId: new FormControl(null, [
             Validators.required
      ]),

      photo: new FormControl(null)
    });
  }

  onFileSelected(event) {
    this.selectedPhoto = event.target.files[0];
    if (!(this.selectedPhoto.name.endsWith('.png') || this.selectedPhoto.name.endsWith('.jpg'))) {
      this.providerForm.controls.photo.setErrors({'incorrect photo': true});
      this.selectedPhoto = null;
    }
  }

  createProvider() {
    const providerData = new FormData();
    const category = this.categoryService.findById(this.providerForm.get('categoryId').value);

    const provider = new ProviderModel();
    provider.name = this.providerForm.get(ProviderConstants.NAME).value;
    provider.price = this.providerForm.get(ProviderConstants.PRICE).value;
    provider.description = this.providerForm.get(ProviderConstants.DESCRIPTION).value;
    provider.category = category;
    provider.ownerBillingId = this.providerForm.get(ProviderConstants.OWNER_ACCOUNT_ID).value;

    providerData.append(ProviderConstants.PHOTO, this.selectedPhoto);
    providerData.append('provider', JSON.stringify(provider));

    this.subscribes.push(this.providerService.createProvider(providerData).subscribe(res => {
      this.router.navigate(['../services/' + res.id]);
    }));

  }

  getCategories() {
    return this.categoryService.categories;
  }

  getUserBAList() {
      return this.billingAccounts;
    }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscribes.forEach(s => s.unsubscribe());
  }

  get user() {
    return this.tokenService.userDetails;
  }
}
