import {Component, OnInit} from '@angular/core';
import {ProviderService} from '../../../../service/provider.service';
import {Page} from '../../../../entities/Page';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';
import {HttpParams} from '@angular/common/http';

@Component({
  selector: 'app-view-all-providers',
  templateUrl: './view-all-providers.component.html',
  styleUrls: ['./view-all-providers.component.css']
})

export class ViewAllProvidersComponent implements OnInit {
  public defaultStartPage = 1;
  public pageSize = 5;
  public page: Page;
  public pageNumber: number[] = [];
  public loading = false;
  public subscribes: Subscription[] = [];
  public currentCategoryName: string;
  public sort: { direction: string; sortBy: string } = {direction: 'Asc', sortBy: null};

  constructor(private providerService: ProviderService,
              private activatedRoute: ActivatedRoute) {

  }

  ngOnInit(): void {
    this.loading = true;
    this.subscribeToQueryParam();
  }

  subscribeToQueryParam() {
    const sub = this.activatedRoute.queryParamMap.subscribe(res => {
      this.currentCategoryName = res.get('categoryName');
      this.getPage(this.defaultStartPage, this.pageSize);
      console.log(this.currentCategoryName);
    });
    this.subscribes.push(sub);
  }

  getPage(pageNumber: number, pageSize?: number) {
    const params = this.buildHttpParams(pageNumber, pageSize, this.sort?.direction, this.sort?.sortBy);
    this.providerService.getPage(params).subscribe((page: Page) => {
      this.page = page;
      this.pageNumber = [];
      for (let i = 1; i < this.page?.totalPages + 1; i++) {
        this.pageNumber.push(i);
      }
      this.loading = false;
    });
  }


  private buildHttpParams(pageNumber: number, pageSize?: number, direction?: string, sortBy?: string) {
    pageNumber -= 1;
    let params = new HttpParams()
      .set('pageNumber', pageNumber.toString());

    if (pageSize) {
      params = params.set('pageSize', pageSize.toString());
    } else {
      params = params.set('pageSize', this.pageSize.toString());
    }

    if (this.currentCategoryName) {
      params = params.set('categoryName', this.currentCategoryName);
    }

    if (direction) {
      params = params.set('direction', direction);
    }

    if (sortBy) {
      params = params.set('sortBy', sortBy);
    }

    return params;
  }

  changePageSize(event) {
    this.pageSize = event.target.value;
    this.getPage(this.page.currentPageNumber + 1, this.pageSize);
  }

  sortBy(event) {
    const direction = event.target.value.split(' ')[0];
    const sortBy = event.target.value.split(' ')[1];
    this.sort.direction = direction;
    this.sort.sortBy = sortBy;
    this.getPage(this.defaultStartPage, this.pageSize);
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscribes.forEach(s => s.unsubscribe());
  }
}
