import {Component, OnInit} from '@angular/core';
import {ProviderService} from '../../../../service/provider.service';
import {ProviderModel} from '../../../../entities/provider';
import {ActivatedRoute, Router} from '@angular/router';
import {ProviderCategoryService} from '../../../../service/provider-category.service';
import {BillingAccount} from '../../../../entities/billing-account';
import {BillingAccountService} from '../../../../service/billing-account.service';
import {TokenService} from '../../../../service/token.service';
import {MDBModalRef, MDBModalService} from 'angular-bootstrap-md';
import {SubscribeComponent} from '../../subscriptions/subscribe/subscribe.component';
import {EditProviderComponent} from '../edit-provider/edit-provider.component';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-services',
  templateUrl: './provider.component.html',
  styleUrls: ['./provider.component.css']
})
export class ProviderComponent implements OnInit {
  private providerId: number;
  public provider: ProviderModel;
  public billingAccounts: BillingAccount[];
  public loading = false;
  public modalRef: MDBModalRef;
  private subscribes: Subscription[] = [];


  constructor(private providerService: ProviderService,
              private categoryService: ProviderCategoryService,
              private baService: BillingAccountService,
              private tokenService: TokenService,
              private modalService: MDBModalService,
              private activateRoute: ActivatedRoute,
              private router: Router) {
    this.providerId = activateRoute.snapshot.params.id;
  }

  ngOnInit(): void {
    this.loading = true;
    this.loadProvider(this.providerId);
    this.loadProviderCategories();
    if (this.user?.id) {
      this.getUserBAList();
    }
  }

  loadProvider(id: number) {
    this.subscribes.push(this.providerService.getById(id).subscribe(provider => {
      this.provider = provider as ProviderModel;
      this.loading = false;
    }, error => this.router.navigate(['../not-found'])));
  }

  loadProviderCategories() {
    this.subscribes.push(this.categoryService.getAllCategories().subscribe());
  }

  deleteProvider(id: number) {
    this.subscribes.push(this.providerService.deleteProvider(id)
      .subscribe(res => this.router.navigate(['../services'])));
  }

  getUserBAList() {
    this.subscribes.push(this.baService.getBillingAccountsByUserId(this.user?.id)
      .subscribe(res => this.billingAccounts = res));
  }

  openSubscribeModal() {
    const modalOptionals = {
      data: {
        provider: this.provider,
        billingAccounts: this.billingAccounts
      }
    };
    this.modalRef = this.modalService.show(SubscribeComponent, modalOptionals);
  }

  openEditModal() {
    const modalOptionals = {
      data: {
        provider: this.provider,
      }
    };
    this.modalRef = this.modalService.show(EditProviderComponent, modalOptionals);
  }

  get user() {
    return this.tokenService.userDetails;
  }

  get userRole() {
    return this.user?.authorities[0]?.authority;
  }

  // tslint:disable-next-line:use-lifecycle-interface
  ngOnDestroy() {
    this.subscribes.forEach(s => s.unsubscribe());
  }

}
