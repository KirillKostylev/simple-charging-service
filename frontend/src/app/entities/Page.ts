import {ProviderModel} from './provider';

export class Page {
  content: ProviderModel[];
  totalElements: number;
  totalPages: number;
  numberOfElements: number;
  currentPageNumber: number;
  last: boolean;
  first: boolean;
}
