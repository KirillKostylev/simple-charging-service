import {Provider} from '@angular/core';
import {BillingAccount} from './billing-account';
import {SubscriptionStatus} from './subscription-status';

export class Subscription {
  id: number;
  provider: Provider;
  billingAccount: BillingAccount;
  providerId: number;
  billingAccountId: number;
  startTime: Date;
  endTime: Date;
  status: SubscriptionStatus;
  daysNumber: number;
}
