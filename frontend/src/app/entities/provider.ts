import {ProviderCategory} from './provider-category';

export enum ProviderConstants {
  ID = 'id',
  NAME = 'name',
  DESCRIPTION = 'description',
  PHOTO = 'photo',
  PRICE = 'price',
  CATEGORY = 'category',
  OWNER_ACCOUNT_ID = 'ownerBillingAccountId'
}

export class ProviderModel {
  id: number;
  name: string;
  description: string;
  price: number;
  photo: string;
  category: ProviderCategory;
  ownerBillingId: number;

  constructor() {
  }

}
