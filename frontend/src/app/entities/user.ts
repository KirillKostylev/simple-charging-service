import {Role} from './role';

export enum UserConstants {
  ID = 'id',
  LOGIN = 'login',
  PASSWORD = 'password',
  FIRST_NAME = 'firstName',
  LAST_NAME = 'lastName',
  ROLE = 'role'
}

export class User {
  id: number;
  login: string;
  password: string;
  firstName: string;
  lastName: string;
  role: Role;
  scount: number;

  constructor() {
  }
}
