export class BillingAccount {
  id: number;
  balance: number;
  additionalQuantity: number;

  constructor(id: number, additionalQuantity: number) {
    this.id = id;
    this.additionalQuantity = additionalQuantity;
  }
}
