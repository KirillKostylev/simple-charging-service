package com.netcracker.fapi;

import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class FapiApplication {
    @Bean
    public ModelMapper createMapper() {
        return new ModelMapper();
    }

    public static void main(String[] args) {
        SpringApplication.run(FapiApplication.class, args);
    }
}
