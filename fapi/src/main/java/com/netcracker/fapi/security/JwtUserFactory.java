package com.netcracker.fapi.security;

import com.netcracker.fapi.entity.Role;
import com.netcracker.fapi.entity.UserAccount;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.util.HashSet;
import java.util.Set;

public final class JwtUserFactory {
    public static JwtUser create(UserAccount user) {
        return new JwtUser(
                user.getId(),
                user.getLogin(),
                user.getPassword(),
                getAuthority(user.getRole()),
                user.getFirstName(),
                user.getLastName()
        );
    }


    private static Set<SimpleGrantedAuthority> getAuthority(Role role) {
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + role.getName()));
        return authorities;
    }
}
