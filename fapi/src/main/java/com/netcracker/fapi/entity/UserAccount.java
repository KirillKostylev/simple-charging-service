package com.netcracker.fapi.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
public class UserAccount {
    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private Role role;
    private Set<BillingAccount> billingAccounts = new HashSet<>();
}
