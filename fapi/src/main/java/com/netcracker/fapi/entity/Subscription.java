package com.netcracker.fapi.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@Setter
@Getter
@NoArgsConstructor
public class Subscription {
    private Long id;
    private Timestamp startTime;
    private Timestamp endTime;
    private BillingAccount billingAccount;
    private Provider provider;
    private SubscriptionStatus status;
}
