package com.netcracker.fapi.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Blob;

@Setter
@Getter
@NoArgsConstructor
public class Provider {
    private Long id;
    private String name;
    private String description;
    private double price;
    private byte[] photo = null;
    private ProviderCategory category;
    private BillingAccount ownerBillingAccount;
}
