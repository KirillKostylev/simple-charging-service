package com.netcracker.fapi.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@NoArgsConstructor
public class ProviderCategory {
    private Long id;
    private String name;
}
