package com.netcracker.fapi.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@Setter
@Getter
@NoArgsConstructor
public class BillingAccount {
    private Long id;
    private double balance = 0;
    private UserAccount userAccount;
    private Set<Subscription> subscriptions = new HashSet<>();
}
