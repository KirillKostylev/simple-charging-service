package com.netcracker.fapi.controller;

import com.netcracker.fapi.dto.ProviderCategoryDto;
import com.netcracker.fapi.service.ProviderCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/fapi/service-categories")
public class ProviderCategoryController {
    @Autowired
    private ProviderCategoryService providerCategoryService;

    @GetMapping
    public ResponseEntity<Iterable<ProviderCategoryDto>> getAllCategories() {
        List<ProviderCategoryDto> all = providerCategoryService.findAll();
        return ResponseEntity.ok(all);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PostMapping
    public ResponseEntity<ProviderCategoryDto> create(@RequestBody ProviderCategoryDto providerCategoryDto) {
        providerCategoryDto = providerCategoryService.create(providerCategoryDto);
        return new ResponseEntity<>(providerCategoryDto, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @PutMapping
    public ResponseEntity<ProviderCategoryDto> update(@RequestBody ProviderCategoryDto providerCategoryDto) {
        providerCategoryDto = providerCategoryService.update(providerCategoryDto);
        return ResponseEntity.ok(providerCategoryDto);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        providerCategoryService.deleteById(id);
    }

}
