package com.netcracker.fapi.controller;

import com.netcracker.fapi.dto.UserAccountDto;
import com.netcracker.fapi.service.UserAccountService;
import com.netcracker.fapi.service.exception.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Role;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/fapi/users")
public class UserAccountController {
    @Autowired
    private UserAccountService userAccountService;

    @PreAuthorize("hasRole('ADMIN')")
    @GetMapping()
    public ResponseEntity<List<UserAccountDto>> findAll() {
        List<UserAccountDto> all = userAccountService.findAll();
        return ResponseEntity.ok(all);
    }

    @PreAuthorize("hasRole('ADMIN')")
    @DeleteMapping("/{id}")
    public void deleteUserById(@PathVariable Long id) {
        userAccountService.deleteById(id);
    }

    @GetMapping("/{id}")
    public ResponseEntity<UserAccountDto> getUserById(@PathVariable Long id) {
        UserAccountDto dto = userAccountService.findById(id);
        return ResponseEntity.ok(dto);
    }


    @PutMapping
    public UserAccountDto update(@RequestBody UserAccountDto userAccountDto) {
        userAccountDto = userAccountService.update(userAccountDto);
        return userAccountDto;
    }

    @PostMapping
    public ResponseEntity<UserAccountDto> create(@RequestBody UserAccountDto userAccountDto) {
        userAccountDto = userAccountService.create(userAccountDto);
        return new ResponseEntity<>(userAccountDto, HttpStatus.CREATED);
    }


    @GetMapping("/login/{login}")
    public ResponseEntity<UserAccountDto> findByLogin(@PathVariable String login) throws EntryNotFoundException {
        UserAccountDto byLogin = userAccountService.checkingUserExistByLogin(login);
        return ResponseEntity.ok(byLogin);
    }


}
