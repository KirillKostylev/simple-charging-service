package com.netcracker.fapi.controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netcracker.fapi.dto.PageDto;
import com.netcracker.fapi.dto.ProviderDto;
import com.netcracker.fapi.service.ProviderService;
import com.netcracker.fapi.util.Converter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

@CrossOrigin
@RestController
@RequestMapping("/api/fapi/services")
public class ProviderController {
    @Autowired
    private ProviderService providerService;

    @GetMapping("{id}")
    public ResponseEntity<ProviderDto> findById(@PathVariable Long id) {
        ProviderDto providerDto = providerService.findById(id);
        return ResponseEntity.ok(providerDto);
    }

    @GetMapping("/pages")
    public ResponseEntity<PageDto> findPage(
            @RequestParam("pageNumber") int pageNumber,
            @RequestParam("pageSize") int pageSize,
            @RequestParam(value = "categoryName", required = false) String categoryName,
            @RequestParam(value = "direction", required = false) String direction,
            @RequestParam(value = "sortBy", required = false) String sortBy) {
        PageDto pageDto = providerService.findPage(pageNumber, pageSize, categoryName, direction, sortBy);
        return ResponseEntity.ok(pageDto);
    }

    @GetMapping
    public ResponseEntity<List<ProviderDto>> findAll() {
        List<ProviderDto> providers = providerService.findAll();
        return ResponseEntity.ok(providers);
    }

    @GetMapping("/category/{name}")
    public ResponseEntity<List<ProviderDto>> findAllByCategoryName(@PathVariable String name) {
        List<ProviderDto> providers = providerService.findAllByCategoryName(name);
        return ResponseEntity.ok(providers);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('PROVIDER')")
    @PostMapping()
    public ResponseEntity<ProviderDto> create(@RequestParam(value = "photo", required = false) MultipartFile photo,
                                              @RequestParam("provider") String provider) throws IOException {

        provider = optimizeProvider(provider);
        ProviderDto dto = new ObjectMapper().readValue(provider, ProviderDto.class);
        if (photo != null) {
            dto.setPhoto(Converter.convertByteArrayToBase64(photo.getBytes()));
        }
        dto = providerService.create(dto);
        if (dto != null) {
            return new ResponseEntity<>(dto, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }


    @PreAuthorize("hasRole('ADMIN') or hasRole('PROVIDER')")
    @PutMapping()
    public ResponseEntity<ProviderDto> update(@RequestParam(value = "photo", required = false) MultipartFile photo,
                                              @RequestParam("provider") String provider) throws IOException {
        provider = optimizeProvider(provider);
        ProviderDto dto = new ObjectMapper().readValue(provider, ProviderDto.class);
        if (photo != null) {
            dto.setPhoto(Converter.convertByteArrayToBase64(photo.getBytes()));
        }
        dto = providerService.update(dto);
        if (dto != null) {
            return new ResponseEntity<>(dto, HttpStatus.CREATED);
        }
        return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('PROVIDER')")
    @DeleteMapping("/{id}")
    public void deleteById(@PathVariable Long id) {
        providerService.deleteById(id);
    }


    private String optimizeProvider(@RequestParam("provider") String provider) {
        provider = provider.replace("\"photo\":{},", "");
        return provider;
    }

}
