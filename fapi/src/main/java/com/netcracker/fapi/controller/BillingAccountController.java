package com.netcracker.fapi.controller;

import com.netcracker.fapi.dto.BillingAccountDto;
import com.netcracker.fapi.dto.UserAccountDto;
import com.netcracker.fapi.service.BillingAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/fapi/billing-accounts")
public class BillingAccountController {
    @Autowired
    private BillingAccountService billingAccountService;

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @GetMapping("/{id}")
    public ResponseEntity<BillingAccountDto> getById(@PathVariable Long id) {
        BillingAccountDto dto = billingAccountService.findById(id);
        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @PostMapping
    public ResponseEntity<BillingAccountDto> addBillingAccount(@RequestBody UserAccountDto userAccountDto) {
        BillingAccountDto billingAccountDto = billingAccountService.addBillingAccount(userAccountDto);
        return new ResponseEntity<>(billingAccountDto, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @PutMapping
    public ResponseEntity<BillingAccountDto> update(@RequestBody BillingAccountDto dto) {
        billingAccountService.update(dto);
        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @DeleteMapping("/{id}")
    public void delete(@PathVariable Long id) {
        billingAccountService.deleteById(id);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @GetMapping("/users/{id}")
    public ResponseEntity<List<BillingAccountDto>> getAllBillingAccountsByUserId(@PathVariable Long id) {
        List<BillingAccountDto> all = billingAccountService.getAllBillingAccountsByUserId(id);
        return ResponseEntity.ok(all);
    }


}
