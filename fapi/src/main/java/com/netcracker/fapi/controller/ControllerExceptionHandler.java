package com.netcracker.fapi.controller;

import com.netcracker.fapi.service.exception.EntryNotFoundException;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@ControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(EntryNotFoundException.class)
    protected ResponseEntity<OutputException> handleEntryNotFoundException(EntryNotFoundException e) {
        return new ResponseEntity<>(new OutputException("User not found"), HttpStatus.OK);
    }

    @ExceptionHandler(BadCredentialsException.class)
    protected ResponseEntity<OutputException> handleBadCredentialsException(BadCredentialsException e) {
        return new ResponseEntity<>(new OutputException(e.getMessage()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(AuthenticationCredentialsNotFoundException.class)
    protected ResponseEntity<OutputException> handleAuthenticationCredentialsNotFoundException(AuthenticationCredentialsNotFoundException e) {
        return new ResponseEntity<>(new OutputException(e.getMessage()), HttpStatus.OK);
    }

    @Data
    @AllArgsConstructor
    private static class OutputException {
        private String message;
    }
}
