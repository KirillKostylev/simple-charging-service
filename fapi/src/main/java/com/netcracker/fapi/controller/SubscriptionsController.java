package com.netcracker.fapi.controller;

import com.netcracker.fapi.dto.SubscriptionDto;
import com.netcracker.fapi.service.SubscriptionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("api/fapi/subscriptions/")
public class SubscriptionsController {
    @Autowired
    private SubscriptionsService subscriptionsService;

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @GetMapping("{id}")
    public ResponseEntity<SubscriptionDto> findById(@PathVariable Long id) {
        SubscriptionDto subscriptionDto = subscriptionsService.findById(id);
        return ResponseEntity.ok(subscriptionDto);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @DeleteMapping("{id}")
    public void deleteById(@PathVariable Long id) {
        subscriptionsService.deleteById(id);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @PostMapping
    public ResponseEntity<SubscriptionDto> create(@RequestBody SubscriptionDto dto) {
        dto = subscriptionsService.create(dto);
        return new ResponseEntity<>(dto, HttpStatus.CREATED);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @PutMapping
    public ResponseEntity<SubscriptionDto> update(@RequestBody SubscriptionDto dto) {
        dto = subscriptionsService.update(dto);
        return ResponseEntity.ok(dto);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @PutMapping("status")
    public ResponseEntity<SubscriptionDto> changeStatus(@RequestBody SubscriptionDto dto) {
        SubscriptionDto subscriptionDto = subscriptionsService.changeStatus(dto);
        return ResponseEntity.ok(subscriptionDto);
    }

    @PreAuthorize("hasRole('ADMIN') or hasRole('CLIENT') or hasRole('PROVIDER')")
    @GetMapping("users/{id}")
    public ResponseEntity<List<SubscriptionDto>> getAllSubscriptionsByUserId(@PathVariable Long id) {
        List<SubscriptionDto> subscriptions = subscriptionsService.getAllSubscriptionsByUserId(id);
        return ResponseEntity.ok(subscriptions);
    }

}
