package com.netcracker.fapi.controller;

import com.netcracker.fapi.dto.UserAccountDto;
import com.netcracker.fapi.entity.AuthToken;
import com.netcracker.fapi.security.JwtUser;
import com.netcracker.fapi.security.TokenProvider;
import com.netcracker.fapi.service.UserAccountService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationCredentialsNotFoundException;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.annotation.*;

//AuthenticationController has API exposed to generate JWT token
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping("/api/fapi/token")
public class AuthenticationController {
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationController.class);
    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private TokenProvider tokenProvider;


    @PostMapping("/generate-token")
    public ResponseEntity generateToken(@RequestBody UserAccountDto user) {
        String login = user.getLogin();
        try {
            final Authentication authentication = authenticationManager.authenticate(
                    new UsernamePasswordAuthenticationToken(
                            login,
                            user.getPassword()
                    ));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            final String token = tokenProvider.generateToken(authentication);
            LOGGER.info("Token was generate");
            return ResponseEntity.ok(new AuthToken(token));
        } catch (AuthenticationException e) {
            throw new BadCredentialsException("Invalid login or password");
        }
    }

    @GetMapping("/details")
    public ResponseEntity getUserDetails() {
        SecurityContext context = SecurityContextHolder.getContext();
        try {
            Authentication authentication = context.getAuthentication();
            JwtUser details = (JwtUser) authentication.getPrincipal();
            return ResponseEntity.ok(details);
        }catch (ClassCastException e){
            throw new AuthenticationCredentialsNotFoundException("User details not found");
        }

    }
}
