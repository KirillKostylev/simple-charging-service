package com.netcracker.fapi.dto.mapper;

import java.util.List;
import java.util.stream.Collectors;

public interface Mapper<ENTITY, DTO> {
    ENTITY toEntity(DTO dto);

    DTO toDto(ENTITY entity);

    default List<DTO> convertToDtoList(List<ENTITY> roles) {
        return roles.stream()
                .map(this::toDto)
                .collect(Collectors.toList());
    }
}
