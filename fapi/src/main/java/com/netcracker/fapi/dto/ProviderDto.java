package com.netcracker.fapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.netcracker.fapi.entity.BillingAccount;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.sql.Blob;

@NoArgsConstructor
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ProviderDto {
    private Long id;
    private String name;
    private String description;
    private double price;
    private String photo;
    private ProviderCategoryDto category;
    private Long ownerBillingAccountId;

}
