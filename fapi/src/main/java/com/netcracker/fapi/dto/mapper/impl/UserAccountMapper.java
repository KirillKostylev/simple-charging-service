package com.netcracker.fapi.dto.mapper.impl;

import com.netcracker.fapi.dto.UserAccountDto;
import com.netcracker.fapi.dto.mapper.Mapper;
import com.netcracker.fapi.entity.UserAccount;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserAccountMapper implements Mapper<UserAccount, UserAccountDto> {
    @Autowired
    private ModelMapper mapper;

    @Override
    public UserAccount toEntity(UserAccountDto dto) {
        return dto == null ? null : mapper.map(dto, UserAccount.class);
    }

    @Override
    public UserAccountDto toDto(UserAccount entity) {
        return entity == null ? null : mapper.map(entity, UserAccountDto.class);
    }
}
