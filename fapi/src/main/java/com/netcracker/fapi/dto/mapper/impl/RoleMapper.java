package com.netcracker.fapi.dto.mapper.impl;

import com.netcracker.fapi.dto.RoleDto;
import com.netcracker.fapi.dto.mapper.Mapper;
import com.netcracker.fapi.entity.Role;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class RoleMapper implements Mapper<Role, RoleDto> {
    @Autowired
    private ModelMapper mapper;

    public Role toEntity(RoleDto dto) {
        return dto == null ? null : mapper.map(dto, Role.class);
    }

    public RoleDto toDto(Role entity) {
        return entity == null ? null : mapper.map(entity, RoleDto.class);
    }

}
