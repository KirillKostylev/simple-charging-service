package com.netcracker.fapi.dto.mapper.impl;

import com.netcracker.fapi.dto.ProviderCategoryDto;
import com.netcracker.fapi.dto.mapper.Mapper;
import com.netcracker.fapi.entity.ProviderCategory;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProviderCategoryMapper implements Mapper<ProviderCategory, ProviderCategoryDto> {
    @Autowired
    private ModelMapper mapper;

    public ProviderCategory toEntity(ProviderCategoryDto dto) {
        return dto == null ? null : mapper.map(dto, ProviderCategory.class);
    }

    public ProviderCategoryDto toDto(ProviderCategory entity) {
        return entity == null ? null : mapper.map(entity, ProviderCategoryDto.class);
    }
}
