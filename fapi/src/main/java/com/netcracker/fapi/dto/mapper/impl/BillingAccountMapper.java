package com.netcracker.fapi.dto.mapper.impl;

import com.netcracker.fapi.dto.BillingAccountDto;
import com.netcracker.fapi.dto.mapper.Mapper;
import com.netcracker.fapi.entity.BillingAccount;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class BillingAccountMapper implements Mapper<BillingAccount, BillingAccountDto> {
    @Autowired
    private ModelMapper mapper;

    public BillingAccount toEntity(BillingAccountDto dto) {
        mapper.getConfiguration().setAmbiguityIgnored(true);
        return dto == null ? null : mapper.map(dto, BillingAccount.class);
    }

    public BillingAccountDto toDto(BillingAccount entity) {
        return entity == null ? null : mapper.map(entity, BillingAccountDto.class);
    }
}
