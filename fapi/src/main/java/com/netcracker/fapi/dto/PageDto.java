package com.netcracker.fapi.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Page;

import java.util.List;

@Setter
@Getter
@NoArgsConstructor
public class PageDto {
    private List content;
    private Long totalElements;
    private int totalPages;
    private int numberOfElements;
    private int currentPageNumber;
    private boolean last;
    private boolean first;

    public PageDto(Page page) {
        this.content = page.getContent();
        this.totalElements = page.getTotalElements();
        this.totalPages = page.getTotalPages();
        this.numberOfElements = page.getNumberOfElements();
        this.currentPageNumber = page.getNumber();
        this.first = page.isFirst();
        this.last = page.isLast();
    }
}
