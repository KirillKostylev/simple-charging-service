package com.netcracker.fapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.sql.Timestamp;

@NoArgsConstructor
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class SubscriptionDto {
    private Long id;
    private Timestamp startTime;
    private Timestamp endTime;
    private int daysNumber;
    private BillingAccountDto billingAccount;
    private long billingAccountId;
    private long providerId;
    private ProviderDto provider;
    private SubscriptionStatusDto status;
}
