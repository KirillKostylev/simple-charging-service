package com.netcracker.fapi.dto.mapper.impl;

import com.netcracker.fapi.dto.ProviderDto;
import com.netcracker.fapi.dto.mapper.Mapper;
import com.netcracker.fapi.entity.Provider;
import com.netcracker.fapi.util.Converter;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class ProviderMapper implements Mapper<Provider, ProviderDto> {
    @Autowired
    private ModelMapper mapper;

    public Provider toEntity(ProviderDto dto) {

        if (dto == null) {
            return null;
        }
        Provider entity = mapper.map(dto, Provider.class);
        entity.setPhoto(Converter.convertBase64ToByteArray(dto.getPhoto()));
        return entity;
    }

    public ProviderDto toDto(Provider entity) {
        if (entity == null) {
            return null;
        }
        ProviderDto dto = mapper.map(entity, ProviderDto.class);
        dto.setPhoto(Converter.convertByteArrayToBase64(entity.getPhoto()));
        return dto;
    }
}
