package com.netcracker.fapi.dto.mapper.impl;

import com.netcracker.fapi.dto.SubscriptionStatusDto;
import com.netcracker.fapi.dto.mapper.Mapper;
import com.netcracker.fapi.entity.SubscriptionStatus;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;

public class SubscriptionStatusMapper implements Mapper<SubscriptionStatus, SubscriptionStatusDto> {
    @Autowired
    private ModelMapper mapper;

    @Override
    public SubscriptionStatus toEntity(SubscriptionStatusDto dto) {
        return dto == null ? null : mapper.map(dto, SubscriptionStatus.class);
    }

    @Override
    public SubscriptionStatusDto toDto(SubscriptionStatus entity) {
        return entity == null ? null : mapper.map(entity, SubscriptionStatusDto.class);
    }
}
