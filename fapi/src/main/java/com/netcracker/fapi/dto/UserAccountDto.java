package com.netcracker.fapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class UserAccountDto {
    private Long id;
    private String login;
    private String password;
    private String firstName;
    private String lastName;
    private RoleDto role;
    private Integer sCount;
//    private Set<BillingAccountDto> billingAccounts = new HashSet<>();
}
