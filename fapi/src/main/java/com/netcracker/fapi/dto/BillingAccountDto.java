package com.netcracker.fapi.dto;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@Setter
@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
public class BillingAccountDto {
    private Long id;
    private double balance = 0;
    private UserAccountDto userAccount;
    private Set<SubscriptionDto> subscriptions = new HashSet<>();
    private double additionalQuantity;
}
