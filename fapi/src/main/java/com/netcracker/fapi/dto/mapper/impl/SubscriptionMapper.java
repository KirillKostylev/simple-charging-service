package com.netcracker.fapi.dto.mapper.impl;

import com.netcracker.fapi.dto.BillingAccountDto;
import com.netcracker.fapi.dto.ProviderDto;
import com.netcracker.fapi.dto.SubscriptionDto;
import com.netcracker.fapi.dto.mapper.Mapper;
import com.netcracker.fapi.entity.BillingAccount;
import com.netcracker.fapi.entity.Provider;
import com.netcracker.fapi.entity.Subscription;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class SubscriptionMapper implements Mapper<Subscription, SubscriptionDto> {
    @Autowired
    private ModelMapper mapper;

    @Autowired
    private ProviderMapper providerMapper;

    @Autowired
    private BillingAccountMapper billingAccountMapper;


    public Subscription toEntity(SubscriptionDto dto) {
        mapper.getConfiguration().setAmbiguityIgnored(true);
        if (dto == null) {
            return null;
        }
        Subscription entity = mapper.map(dto, Subscription.class);

        Provider providerEntity = providerMapper.toEntity(dto.getProvider());
        BillingAccount baEntity = billingAccountMapper.toEntity(dto.getBillingAccount());

        entity.setProvider(providerEntity);
        entity.setBillingAccount(baEntity);

        return entity;
    }

    public SubscriptionDto toDto(Subscription entity) {
        if (entity == null) {
            return null;
        }
        SubscriptionDto dto = mapper.map(entity, SubscriptionDto.class);

        ProviderDto providerDto = providerMapper.toDto(entity.getProvider());
        BillingAccountDto baDto = billingAccountMapper.toDto(entity.getBillingAccount());

        dto.setProvider(providerDto);
        dto.setBillingAccount(baDto);

        return dto;
    }
}
