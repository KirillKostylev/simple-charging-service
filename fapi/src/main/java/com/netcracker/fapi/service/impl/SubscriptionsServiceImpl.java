package com.netcracker.fapi.service.impl;

import com.netcracker.fapi.dto.BillingAccountDto;
import com.netcracker.fapi.dto.ProviderDto;
import com.netcracker.fapi.dto.SubscriptionDto;
import com.netcracker.fapi.dto.mapper.impl.SubscriptionMapper;
import com.netcracker.fapi.entity.Subscription;
import com.netcracker.fapi.service.BillingAccountService;
import com.netcracker.fapi.service.ProviderService;
import com.netcracker.fapi.service.SubscriptionsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class SubscriptionsServiceImpl implements SubscriptionsService {
    @Value("${backend.server.url}" + "${backend.subscriptions.url}")
    private String backendSubscriptionsUrl;

    @Value("${backend.users.url}")
    private String usersUrl;

    @Autowired
    private SubscriptionMapper subscriptionMapper;

    @Autowired
    private BillingAccountService billingAccountService;

    @Autowired
    private ProviderService providerService;

    @Override
    public SubscriptionDto create(SubscriptionDto subscriptionDto) {
        RestTemplate restTemplate = new RestTemplate();

        ProviderDto providerDto = providerService.findById(subscriptionDto.getProviderId());
        BillingAccountDto billingAccountDto = billingAccountService.findById(subscriptionDto.getBillingAccountId());

        subscriptionDto.setBillingAccount(billingAccountDto);
        subscriptionDto.setProvider(providerDto);


        Subscription subscription = subscriptionMapper.toEntity(subscriptionDto);
        subscription = restTemplate.postForObject(backendSubscriptionsUrl, subscription, Subscription.class);
        return subscriptionMapper.toDto(subscription);
    }

    @Override
    public SubscriptionDto findById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        Subscription subscription = restTemplate.getForObject(backendSubscriptionsUrl + id, Subscription.class);
        return subscriptionMapper.toDto(subscription);
    }

    @Override
    public void deleteById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendSubscriptionsUrl + id);
    }

    @Override
    public SubscriptionDto update(SubscriptionDto subscriptionDto) {
        RestTemplate restTemplate = new RestTemplate();
        Subscription subscription = subscriptionMapper.toEntity(subscriptionDto);
        restTemplate.put(backendSubscriptionsUrl, subscription);
        return findById(subscription.getId());
    }

    @Override
    public List<SubscriptionDto> getAllSubscriptionsByUserId(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        Subscription[] subscriptions =
                restTemplate.getForObject(backendSubscriptionsUrl + usersUrl + id, Subscription[].class);
        List<Subscription> subscriptionList =
                subscriptions == null ? Collections.emptyList() : Arrays.asList(subscriptions);

        List<SubscriptionDto> subscriptionDtos = subscriptionMapper.convertToDtoList(subscriptionList);
        subscriptionDtos.forEach(s -> s.setDaysNumber(calculateWorkDaysNumber(s)));
        return subscriptionDtos;
    }

    @Override
    public List<SubscriptionDto> findAll() {
        return null;
    }

    @Override
    public SubscriptionDto changeStatus(SubscriptionDto subscriptionDto) {
        RestTemplate restTemplate = new RestTemplate();
        Subscription subscription = subscriptionMapper.toEntity(subscriptionDto);
        restTemplate.put(backendSubscriptionsUrl + "status", subscription);
        return findById(subscription.getId());
    }

    private int calculateWorkDaysNumber(SubscriptionDto s) {
        LocalDateTime localDateEndTime = s.getEndTime().toLocalDateTime();
        LocalDateTime localDateStartTime = s.getStartTime().toLocalDateTime();
        Duration duration = Duration.between(localDateStartTime, localDateEndTime);
        return (int) Math.abs(duration.toDays());

    }
}
