package com.netcracker.fapi.service;

import com.netcracker.fapi.dto.UserAccountDto;
import com.netcracker.fapi.entity.UserAccount;
import com.netcracker.fapi.service.exception.EntryNotFoundException;

public interface UserAccountService extends Service<UserAccountDto, Long> {
    UserAccount findByLogin(String login);

    UserAccountDto checkingUserExistByLogin(String login) throws EntryNotFoundException;

    Integer getSubsciptionsCountFromUserById(Long id);
}
