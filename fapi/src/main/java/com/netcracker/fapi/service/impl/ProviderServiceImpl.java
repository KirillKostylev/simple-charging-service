package com.netcracker.fapi.service.impl;

import com.netcracker.fapi.dto.PageDto;
import com.netcracker.fapi.dto.ProviderDto;
import com.netcracker.fapi.dto.mapper.impl.ProviderMapper;
import com.netcracker.fapi.entity.Provider;
import com.netcracker.fapi.service.ProviderService;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Log4j2
@Service
public class ProviderServiceImpl implements ProviderService {
    @Value("${backend.server.url}" + "${backend.services.url}")
    private String backendServicesUrl;

    @Autowired
    private ProviderMapper providerMapper;

    @Override
    public ProviderDto create(ProviderDto providerDto) {
        RestTemplate restTemplate = new RestTemplate();
        Provider provider = providerMapper.toEntity(providerDto);
        provider = restTemplate.postForObject(backendServicesUrl, provider, Provider.class);
        return providerMapper.toDto(provider);
    }

    @Override
    public ProviderDto findById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        Provider provider = restTemplate.getForEntity(backendServicesUrl + id, Provider.class).getBody();
        return providerMapper.toDto(provider);
    }

    @Override
    public void deleteById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendServicesUrl + id);
    }

    @Override
    public ProviderDto update(ProviderDto providerDto) {
        RestTemplate restTemplate = new RestTemplate();
        Provider provider = providerMapper.toEntity(providerDto);
        restTemplate.put(backendServicesUrl, provider);
        return findById(provider.getId());
    }

    @Override
    public PageDto findPage(int pageNumber, int pageSize, String categoryName, String direction, String sortBy) {
        RestTemplate restTemplate = new RestTemplate();
        String url = backendServicesUrl + "/pages/" + "?pageNumber=" + pageNumber + "&pageSize=" + pageSize;
        if (categoryName != null) {
            url += "&categoryName=" + categoryName;
        }

        if (direction != null) {
            url += "&direction=" + direction;
        }

        if (sortBy != null) {
            url += "&sortBy=" + sortBy;
        }

        return restTemplate.getForObject(url, PageDto.class);
    }

    @Override
    public List<ProviderDto> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        Provider[] providers = restTemplate.getForObject(backendServicesUrl, Provider[].class);
        List<Provider> providersList = providers == null ? Collections.emptyList() : Arrays.asList(providers);
        return providerMapper.convertToDtoList(providersList);
    }

    @Override
    public List<ProviderDto> findAllByCategoryName(String name) {
        RestTemplate restTemplate = new RestTemplate();
        Provider[] providers = restTemplate.getForObject(backendServicesUrl + "/category/" + name, Provider[].class);
        List<Provider> providersList = providers == null ? Collections.emptyList() : Arrays.asList(providers);
        return providerMapper.convertToDtoList(providersList);
    }

}
