package com.netcracker.fapi.service;

import com.netcracker.fapi.dto.SubscriptionDto;
import com.netcracker.fapi.entity.Subscription;

import java.util.List;

public interface SubscriptionsService extends Service<SubscriptionDto, Long> {
    List<SubscriptionDto> getAllSubscriptionsByUserId(Long id);

    SubscriptionDto changeStatus(SubscriptionDto subscription);
}
