package com.netcracker.fapi.service;

import com.netcracker.fapi.dto.ProviderCategoryDto;
import com.netcracker.fapi.entity.ProviderCategory;

public interface ProviderCategoryService extends Service<ProviderCategoryDto, Long> {
}
