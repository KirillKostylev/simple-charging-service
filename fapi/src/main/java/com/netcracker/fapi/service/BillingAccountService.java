package com.netcracker.fapi.service;

import com.netcracker.fapi.dto.BillingAccountDto;
import com.netcracker.fapi.dto.UserAccountDto;

import java.util.List;

public interface BillingAccountService extends Service<BillingAccountDto, Long> {
    BillingAccountDto addBillingAccount(UserAccountDto userAccount);

    List<BillingAccountDto> getAllBillingAccountsByUserId(Long id);

}
