package com.netcracker.fapi.service;

import com.netcracker.fapi.dto.PageDto;
import com.netcracker.fapi.dto.ProviderDto;

import java.util.List;

public interface ProviderService extends Service<ProviderDto, Long> {
    List<ProviderDto> findAllByCategoryName(String name);


    PageDto findPage(int pageNumber, int pageSize, String categoryName, String direction, String sortBy);
}
