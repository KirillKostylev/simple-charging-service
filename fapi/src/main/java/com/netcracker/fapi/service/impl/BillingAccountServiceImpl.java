package com.netcracker.fapi.service.impl;

import com.netcracker.fapi.dto.BillingAccountDto;
import com.netcracker.fapi.dto.UserAccountDto;
import com.netcracker.fapi.dto.mapper.impl.BillingAccountMapper;
import com.netcracker.fapi.dto.mapper.impl.UserAccountMapper;
import com.netcracker.fapi.entity.BillingAccount;
import com.netcracker.fapi.entity.UserAccount;
import com.netcracker.fapi.service.BillingAccountService;
import com.netcracker.fapi.service.UserAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class BillingAccountServiceImpl implements BillingAccountService {
    @Value("${backend.server.url}" + "${backend.billing.accounts.url}")
    private String backendBillingAccountsUrl;

    @Value("${backend.users.url}")
    private String usersUrl;

    @Autowired
    private UserAccountService userAccountService;


    @Autowired
    private BillingAccountMapper billingAccountMapper;

    @Autowired
    private UserAccountMapper userAccountMapper;


    @Override
    public BillingAccountDto create(BillingAccountDto dto) {
        return null;
    }

    @Override
    public BillingAccountDto findById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        BillingAccount billingAccount =
                restTemplate.getForEntity(backendBillingAccountsUrl + id, BillingAccount.class).getBody();
        return billingAccountMapper.toDto(billingAccount);
    }

    @Override
    public BillingAccountDto addBillingAccount(UserAccountDto userDto) {
        UserAccountDto fullUserDto = userAccountService.findById(userDto.getId());
        RestTemplate restTemplate = new RestTemplate();
        UserAccount user = userAccountMapper.toEntity(fullUserDto);
        BillingAccount ba =
                restTemplate.postForEntity(backendBillingAccountsUrl, user, BillingAccount.class).getBody();
        return billingAccountMapper.toDto(ba);
    }

    @Override
    public void deleteById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendBillingAccountsUrl + id);
    }

    @Override
    public BillingAccountDto update(BillingAccountDto billingAccountDto) {
        RestTemplate restTemplate = new RestTemplate();

        BillingAccountDto ba = findById(billingAccountDto.getId());
        ba.setBalance(ba.getBalance() + billingAccountDto.getAdditionalQuantity());

        BillingAccount billingAccount = billingAccountMapper.toEntity(ba);
        restTemplate.put(backendBillingAccountsUrl, billingAccount);

        return findById(billingAccount.getId());
    }

    @Override
    public List<BillingAccountDto> getAllBillingAccountsByUserId(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        BillingAccount[] billingAccounts =
                restTemplate.getForObject(backendBillingAccountsUrl + usersUrl + id, BillingAccount[].class);
        List<BillingAccount> billingAccountList =
                billingAccounts == null ? Collections.emptyList() : Arrays.asList(billingAccounts);
        return billingAccountMapper.convertToDtoList(billingAccountList);
    }

    @Override
    public List<BillingAccountDto> findAll() {
        return null;
    }
}
