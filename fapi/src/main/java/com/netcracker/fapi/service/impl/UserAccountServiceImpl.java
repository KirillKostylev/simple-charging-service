package com.netcracker.fapi.service.impl;

import com.netcracker.fapi.dto.UserAccountDto;
import com.netcracker.fapi.dto.mapper.impl.UserAccountMapper;
import com.netcracker.fapi.entity.UserAccount;
import com.netcracker.fapi.security.JwtUserFactory;
import com.netcracker.fapi.service.UserAccountService;
import com.netcracker.fapi.service.exception.EntryNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestClientException;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service("customUserDetailsService")
public class UserAccountServiceImpl implements UserAccountService, UserDetailsService {

    @Value("${backend.server.url}" + "${backend.users.url}")
    private String backendUsersUrl;

    @Autowired
    private UserAccountMapper userAccountMapper;

    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;


    @Override
    public UserAccountDto findById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        UserAccount user = restTemplate.getForObject(backendUsersUrl + id, UserAccount.class);
        return userAccountMapper.toDto(user);
    }

    @Override
    public void deleteById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendUsersUrl + id);
    }

    @Override
    public UserAccountDto update(UserAccountDto userAccountDto) {
        RestTemplate restTemplate = new RestTemplate();
        UserAccount user = userAccountMapper.toEntity(userAccountDto);
        restTemplate.put(backendUsersUrl, user);
        return findById(user.getId());
    }

    @Override
    public List<UserAccountDto> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        UserAccount[] users = restTemplate.getForObject(backendUsersUrl, UserAccount[].class);
        List<UserAccount> userList = users == null ? Collections.emptyList() : Arrays.asList(users);

        List<UserAccountDto> userAccountDtos = userAccountMapper.convertToDtoList(userList);
        for (UserAccountDto u:userAccountDtos) {
            Integer i = getSubsciptionsCountFromUserById(u.getId());
            u.setSCount(i);
        }

        return userAccountDtos;
    }

    @Override
    public UserAccountDto create(UserAccountDto userAccountDto) {
        RestTemplate restTemplate = new RestTemplate();
        UserAccount user = userAccountMapper.toEntity(userAccountDto);
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user = restTemplate.postForObject(backendUsersUrl, user, UserAccount.class);
        return userAccountMapper.toDto(user);
    }


    @Override
    public UserAccountDto checkingUserExistByLogin(String login) throws EntryNotFoundException {
        UserAccount user;
        try {
            user = findByLogin(login);
        } catch (RestClientException e) {
            throw new EntryNotFoundException(e.getMessage());
        }
        UserAccount userLogin = new UserAccount();
        userLogin.setLogin(user.getLogin());
        return userAccountMapper.toDto(userLogin);
    }

    @Override
    public Integer getSubsciptionsCountFromUserById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        Integer i = restTemplate.getForObject(backendUsersUrl + id + "/subscriptions-count", Integer.class);
        return i;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserAccount user = findByLogin(s);
        if (user == null) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }
        return JwtUserFactory.create(user);
//        return new User(user.getLogin(), user.getPassword(), getAuthority(user));
    }

    @Override
    public UserAccount findByLogin(String login) {
        RestTemplate restTemplate = new RestTemplate();
        UserAccount user = restTemplate.getForObject(backendUsersUrl + "/login/" + login, UserAccount.class);
        return user;
    }

//    private Set<SimpleGrantedAuthority> getAuthority(UserAccount user) {
//        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
//        authorities.add(new SimpleGrantedAuthority("ROLE_" + user.getRole().getName()));
//        return authorities;
//    }
}