package com.netcracker.fapi.service;

import java.util.List;

public interface Service<DTO, KEY> {
    DTO create(DTO DTO);

    DTO findById(KEY key);

    void deleteById(KEY key);

    DTO update(DTO DTO);

    List<DTO> findAll();
}
