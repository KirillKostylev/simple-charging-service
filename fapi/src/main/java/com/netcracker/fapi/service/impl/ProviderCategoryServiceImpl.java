package com.netcracker.fapi.service.impl;

import com.netcracker.fapi.dto.ProviderCategoryDto;
import com.netcracker.fapi.dto.mapper.impl.ProviderCategoryMapper;
import com.netcracker.fapi.entity.ProviderCategory;
import com.netcracker.fapi.service.ProviderCategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Service
public class ProviderCategoryServiceImpl implements ProviderCategoryService {

    @Value("${backend.server.url}" + "${backend.service.categories.url}")
    private String backendServiceCategoriesUrl;

    @Autowired
    private ProviderCategoryMapper providerCategoryMapper;

    @Override
    public ProviderCategoryDto create(ProviderCategoryDto providerCategory) {
        RestTemplate restTemplate = new RestTemplate();
        ProviderCategory category = providerCategoryMapper.toEntity(providerCategory);
        category = restTemplate.postForObject(backendServiceCategoriesUrl, category, ProviderCategory.class);
        return providerCategoryMapper.toDto(category);
    }

    @Override
    public ProviderCategoryDto findById(Long id) {
        return null;
    }

    @Override
    public void deleteById(Long id) {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.delete(backendServiceCategoriesUrl + id);
    }

    @Override
    public ProviderCategoryDto update(ProviderCategoryDto providerCategoryDto) {
        RestTemplate restTemplate = new RestTemplate();
        ProviderCategory providerCategory = providerCategoryMapper.toEntity(providerCategoryDto);
        restTemplate.put(backendServiceCategoriesUrl, providerCategory);
        return findById(providerCategory.getId());
    }

    @Override
    public List<ProviderCategoryDto> findAll() {
        RestTemplate restTemplate = new RestTemplate();
        ProviderCategory[] categories =
                restTemplate.getForObject(backendServiceCategoriesUrl, ProviderCategory[].class);

        List<ProviderCategory> categoriesList =
                categories == null ? Collections.emptyList() : Arrays.asList(categories);
        return providerCategoryMapper.convertToDtoList(categoriesList);
    }
}
